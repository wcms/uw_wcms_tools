<?php

/**
 * @file
 * Functions for interacting with the Gitlab API.
 *
 * @see https://git.uwaterloo.ca/help/api/api_resources.md
 * @see https://git.uwaterloo.ca/help/api/branches.md
 * @see https://git.uwaterloo.ca/help/api/projects.md
 * @see https://git.uwaterloo.ca/help/api/README.md
 * @see https://git.uwaterloo.ca/help/api/services.md
 */

require_once 'uw_wcms_tools.lib.inc';

// The URL of the Gitlab server.
define('UW_WCMS_TOOLS_GITLAB_SERVER', 'https://git.uwaterloo.ca/');
// The directory in which to keep clones of repositories.
define('UW_WCMS_TOOLS_REPOSITORY_DIRECTORY', '/fsys1/Repositories');

/**
 * Return the GitLab private token.
 *
 * @return string
 *   The GitLab private token.
 */
function uw_wcms_tools_get_gitlab_private_token() {
  // Gitlab private token. For CLI use, get it from the environment. Otherwise,
  // use the Drupal variable. Almost all the functions in this file depend on
  // having this set.
  if (php_sapi_name() === 'cli') {
    $token = getenv('GITLAB_PRIVATE_TOKEN');
    if ($token === FALSE) {
      throw new Exception('GITLAB_PRIVATE_TOKEN environment variable not set.');
    }
  }
  else {
    $token = variable_get('gitlab_private_token');
    if ($token === NULL) {
      throw new Exception('Drupal variable "gitlab_private_token" not set.');
    }
  }
  return $token;
}

/**
 * Return the URL of a project's Gitlab homepage.
 *
 * @param object $project
 *   A project object.
 *
 * @return string
 *   The URL of a particular project.
 */
function uw_wcms_tools_get_project_url($project) {
  return UW_WCMS_TOOLS_GITLAB_SERVER . $project->path_with_namespace;
}

/**
 * Return the URL of a project's Gitlab homepage by namespace and path.
 *
 * @param string $namespace
 *   The project namespace.
 * @param string $path
 *   The project path.
 *
 * @return string
 *   The URL of a particular project.
 */
function uw_wcms_tools_get_project_url_by_namespace_and_path($namespace, $path) {
  $project = (object) ['path_with_namespace' => $namespace . '/' . $path];
  return uw_wcms_tools_get_project_url($project);
}

/**
 * Query the Gitlab API.
 *
 * @param string $path
 *   The path to query.
 * @param array $query
 *   The query parameters.
 * @param string $method
 *   HTTP method to use. GET, POST, PUT, and DELETE are supported.
 * @param array $content
 *   If $method is "PUT", this value will be JSON-encoded and sent as the body.
 *
 * @return object
 *   The query results.
 */
function uw_wcms_tools_query_gitlab_api($path, array $query = array(), $method = 'GET', array $content = NULL) {
  if (!in_array($method, array('POST', 'PUT', 'DELETE'), TRUE)) {
    $method = 'GET';
  }

  $path = UW_WCMS_TOOLS_GITLAB_SERVER . 'api/v4/' . $path;

  $query = http_build_query($query, '', '&');
  if ($query) {
    $path .= '?' . $query;
  }

  $context = array(
    'http' => array(
      'method'  => $method,
    ),
  );

  $headers = array('PRIVATE-TOKEN: ' . uw_wcms_tools_get_gitlab_private_token());

  if ($method !== 'GET' && $content) {
    $context['http']['content'] = json_encode($content);
    $headers[] = 'Content-Type: application/json';
  }

  $context['http']['header'] = implode("\r\n", $headers);

  $context = stream_context_create($context);
  $data = @file_get_contents($path, FALSE, $context);

  $return = array(
    'http_status' => _uw_wcms_tools_get_http_status($http_response_header),
    'body' => json_decode($data),
  );
  return $return;
}

/**
 * Return a list of the projects.
 *
 * @param array|string|null $namespaces
 *   If a string or array of strings, only return projects in those namespaces.
 *
 * @return array
 *   The array of projects, keyed by namespace and project name.
 */
function uw_wcms_tools_get_projects($namespaces = NULL) {
  // Ensure all namespaces are strings and remove empty names.
  $namespaces = array_map('strval', (array) $namespaces);
  $namespaces = array_filter($namespaces, 'strlen');

  $page = 0;
  $projects = array();
  $page_data = TRUE;
  // Limit to 100 pages.
  while ($page_data && $page < 100) {
    $page++;
    $query = ['per_page' => 100, 'page' => $page];
    $page_data = uw_wcms_tools_query_gitlab_api('projects', $query);
    $page_data = $page_data['body'];
    if (is_array($page_data)) {
      foreach ($page_data as $_) {
        if (!$namespaces || in_array($_->namespace->path, $namespaces, TRUE)) {
          $projects[$_->namespace->path][$_->path] = $_;
        }
      }
    }
  }

  ksort($projects);
  foreach ($projects as &$val) {
    ksort($val);
  }
  unset($val);

  return $projects;
}

/**
 * Loader function for a project object, loading from the API Gitlab API.
 *
 * This can load either by project ID or the namespace and name. If the project
 * is found, it is cached and the same object returned on subsequent calls to
 * load the same project.
 *
 * @param int|string $id_or_namespace
 *   The ID of the project to load or its namespace if loading by name.
 * @param string $project_name
 *   The name of the project to load.
 *
 * @return object|null
 *   The project object or NULL if it cannot be found.
 */
function uw_wcms_tools_get_project($id_or_namespace, $project_name = NULL) {
  if ($project_name) {
    $query = urlencode($id_or_namespace . '/' . $project_name);
  }
  else {
    $query = (int) $id_or_namespace;
  }

  static $cache = [];
  if (isset($cache[$query])) {
    return $cache[$query];
  }

  $project = uw_wcms_tools_query_gitlab_api('projects/' . $query);

  if ($project['http_status'] === 200) {
    $project = $project['body'];
    $cache[$project->id] = $project;
    $cache[urlencode($project->path_with_namespace)] = &$cache[$project->id];
    return $project;
  }
  return NULL;
}

/**
 * Loader function for a GitLab group.
 *
 * @param int|string $id
 *   The integer ID or string path of the group.
 *
 * @return object|null
 *   The group object or NULL if it cannot be found.
 */
function uw_wcms_tools_get_group($id): ?object {
  if (!is_int($id)) {
    $id = urlencode($id);
  }
  $group = uw_wcms_tools_query_gitlab_api('groups/' . $id);
  return ($group['http_status'] === 200) ? $group['body'] : NULL;
}

/**
 * Return the tag in a project which is highest accounting to version_compare().
 *
 * @param int|string $project_id
 *   The project ID or NAMESPACE/PROJECT_PATH of the project to search.
 * @param string $branch_filter
 *   If set to a Drupal branch name (e.g. "7.x-1.x"), remove tags that don't
 *   match (e.g. not like "7.x-1.1").
 * @param bool $full_release_only
 *   When TRUE, alpha, beta, and rc tags will not be returned.
 *
 * @return string|null
 *   The tag or NULL if there are no tags.
 */
function uw_wcms_tools_get_tag_latest($project_id, $branch_filter = NULL, $full_release_only = FALSE) {
  if (!preg_match('/^(\d+|[a-z0-9_]+\/[a-z0-9_]+)$/', $project_id)) {
    return;
  }
  $project_id = urlencode($project_id);

  $tags = uw_wcms_tools_query_gitlab_api('projects/' . $project_id . '/repository/tags', array('per_page' => 100));
  $tags = $tags['body'];

  if (!is_array($tags)) {
    return;
  }

  // If the pattern is a Drupal branch name (e.g. "7.x-1.x"), remove tags that
  // don't match.
  if (preg_match('/^(\d+\.x-\d+\.)(x|\d+)/', $branch_filter, $backref)) {
    $branch_filter = $backref[1];
  }
  elseif (preg_match('/^\d+\.x$/', $branch_filter)) {
    $branch_filter = $branch_filter . '-';
  }
  // Semver filter. Whether given as 1.2.3 or 1.2.x, will filter as "1.2.".
  elseif (preg_match('/^(\d+\.\d+\.)(x|\d+)$/', $branch_filter, $matches)) {
    $branch_filter = $matches[1];
  }
  // Filter on Drupal major core version only by setting it as integer.
  elseif (is_int($branch_filter)) {
    $branch_filter = $branch_filter . '.';
  }
  if ($branch_filter) {
    foreach ($tags as $key => $value) {
      if (strpos($value->name, $branch_filter) !== 0) {
        unset($tags[$key]);
      }
    }
  }

  // If $full_release_only is set, remove alpha, beta, and rc tags unless there
  // are no full releases.
  $tags_new = $tags;
  if ($full_release_only) {
    foreach ($tags as $key => $value) {
      if (preg_match('/(alpha|beta|rc)/', $value->name)) {
        unset($tags_new[$key]);
      }
    }
  }
  if ($tags_new) {
    $tags = $tags_new;
  }

  // Find latest tag.
  $latest = array_shift($tags);
  foreach ($tags as $tag) {
    if (version_compare($tag->name, $latest->name, '>')) {
      $latest = $tag;
    }
  }
  return $latest;
}

/**
 * Return the most recent value in an using version_compare().
 *
 * @param array $versions
 *   An array of versions to compare.
 *
 * @return string|null
 *   The most recent version or NULL if $versions is empty.
 */
function uw_wcms_tools_version_compare_array(array $versions): ?string {
  if (!$versions) {
    return NULL;
  }

  $latest = (string) array_shift($versions);
  foreach ($versions as $version) {
    $version = (string) $version;
    if (version_compare($version, $latest, '>')) {
      $latest = $version;
    }
  }
  return $latest;
}

/**
 * Return the latest commits for the default branch of a project.
 *
 * @param int $project_id
 *   The project ID of the project to search.
 * @param int $per_page
 *   The number of commits to return.
 *
 * @return array|object
 *   An array of commit objects or, if $per_page is 1, that 1 commit object.
 */
function uw_wcms_tools_get_commits($project_id, $per_page = 100) {
  $project_id = (int) $project_id;
  $per_page = (int) $per_page;
  // Will return commits on default branch if no branch or tag specified.
  $commits = uw_wcms_tools_query_gitlab_api('projects/' . $project_id . '/repository/commits', array('per_page' => $per_page));
  $commits = $commits['body'];

  // If they only ask for 1 commit, give it directly instead of an array.
  if ($per_page === 1) {
    return $commits[0];
  }
  return $commits;
}

/**
 * Create a tag via the Gitlab API.
 *
 * @param int $project_id
 *   The project ID of the project to search.
 * @param string $reference
 *   The commit to tag.
 * @param string $tag_name
 *   The name of the tag to create.
 *
 * @return object
 *   The query results object.
 */
function uw_wcms_tools_create_tag($project_id, $reference, $tag_name) {
  $project_id = (int) $project_id;
  $query = array(
    'ref' => $reference,
    'tag_name' => $tag_name,
  );
  return uw_wcms_tools_query_gitlab_api('projects/' . $project_id . '/repository/tags', $query, 'POST');
}

/**
 * Get an array of all WCMS-related projects for converting name to GitLab ID.
 *
 * @param array|string|null $namespaces
 *   If a string or array of strings, only return projects in those namespaces.
 *
 * @return array
 *   Array in the form: project_name => project_id
 */
function uw_wcms_tools_get_project_ids($namespaces = NULL) {
  $project_ids = [];
  foreach (uw_wcms_tools_get_projects($namespaces) as $namespace) {
    foreach ($namespace as $project) {
      $project_ids[$project->path] = $project->id;
    }
  }
  return $project_ids;
}

/**
 * Set the Microsoft Teams Notification integration settings for a project.
 *
 * @param int $id
 *   The GitLab project ID to update.
 *
 * @return array
 *   The API response.
 */
function uw_wcms_tools_gitlab_project_ms_teams_integration_update(int $id): array {
  // Cache update values.
  static $update;
  if (!$update) {
    // Use uw_base_profile as the model.
    $uw_base_profile = uw_wcms_tools_get_project('wcms', 'uw_base_profile');
    $r = uw_wcms_tools_query_gitlab_api('projects/' . $uw_base_profile->id . '/services/microsoft-teams');
    $update = (array) $r['body']->properties;

    // Set desired properties.
    $update['notify_only_broken_pipelines'] = FALSE;
    $update['branches_to_be_notified'] = 'all';
  }

  // Update repository config.
  return uw_wcms_tools_query_gitlab_api('projects/' . $id . '/services/microsoft-teams', [], 'PUT', $update);
}

/**
 * Set project settings.
 *
 * @param int $id
 *   The GitLab project ID.
 *
 * @return array
 *   The API response.
 */
function uw_wcms_tools_gitlab_project_update_features(int $id): array {
  $project = uw_wcms_tools_get_project($id);
  $update = ['id' => $id] + _uw_wcms_tools_gitlab_project_features($project->namespace->path);
  return uw_wcms_tools_query_gitlab_api('projects/' . $id, [], 'PUT', $update);
}

/**
 * Return an array of GitLab project settings.
 *
 * @param string $group
 *   The group to return settings for.
 *
 * @return array
 *   GitLab project settings.
 */
function _uw_wcms_tools_gitlab_project_features(string $group = NULL): array {
  $settings = [
    // Project features.
    'issues_access_level' => 'disabled',
    'repository_access_level' => 'enabled',
    'merge_requests_access_level' => 'enabled',
    'forking_access_level' => 'disabled',
    'builds_access_level' => 'disabled',
    'wiki_access_level' => 'disabled',
    'snippets_access_level' => 'disabled',
    'pages_access_level' => 'disabled',
    'lfs_enabled' => FALSE,
    'packages_enabled' => FALSE,
    // Merge requests.
    'merge_method' => 'rebase_merge',
    'resolve_outdated_diff_discussions' => FALSE,
    'printing_merge_request_link_enabled' => TRUE,
    'remove_source_branch_after_merge' => TRUE,
    'only_allow_merge_if_pipeline_succeeds' => FALSE,
    'allow_merge_on_skipped_pipeline' => FALSE,
    'only_allow_merge_if_all_discussions_are_resolved' => FALSE,
    'suggestion_commit_message' => NULL,
  ];

  // GitLab groups for which to enable CI.
  $ci_groups = [
    'responsive-web-design',
    'wcms-architecture',
    'wcms-decoupled',
    'wcms-openscholar',
    'wcms',
  ];
  if (in_array($group, $ci_groups, TRUE)) {
    $settings['builds_access_level'] = 'private';
    $settings['ci_config_path'] = 'wcms-ci.yml@wcms-automation/wcms-cicd';
  }

  return $settings;
}

/**
 * List project hooks.
 *
 * @param int $id
 *   The GitLab project ID.
 *
 * @return array
 *   The API response.
 */
function uw_wcms_tools_gitlab_project_list_hooks(int $id): array {
  return uw_wcms_tools_query_gitlab_api('projects/' . $id . '/hooks');
}

/**
 * Create a GitLab project with desired settings.
 *
 * @param string $group
 *   The group in which to create the project.
 * @param string $path
 *   The path of the new project.
 *
 * @return array
 *   The API reponse for the project creation.
 *
 * @see https://git.uwaterloo.ca/help/api/projects.md#create-project
 */
function uw_wcms_tools_gitlab_project_create(string $group, string $path): array {
  $group = uw_wcms_tools_get_group($group);

  if (!$group) {
    return [
      'http_status' => NULL,
      'body' => NULL,
      'error' => 'Invalid group',
    ];
  }

  $project = [
    'path' => $path,
    'namespace_id' => $group->id,
    'visibility' => 'public',
    'container_registry_enabled' => FALSE,
  ];
  $project += _uw_wcms_tools_gitlab_project_features($group->path);
  $r = uw_wcms_tools_query_gitlab_api('projects', [], 'POST', $project);

  if ($r['http_status'] === 201) {
    // Settings do not all save on creation, so update them here.
    uw_wcms_tools_gitlab_project_update_features($r['body']->id);
    uw_wcms_tools_gitlab_project_ms_teams_integration_update($r['body']->id);
  }

  return $r;
}

/**
 * Returns an array of branch objects in a project with caching.
 *
 * @param int $project_id
 *   The project ID of the project to search.
 *
 * @return object[]|null
 *   An array of branch objects or NULL on error.
 */
function uw_wcms_tools_get_branches(int $project_id): ?array {
  if ($project_id < 1) {
    return [];
  }
  static $cache = [];
  if (!array_key_exists($project_id, $cache)) {
    $query = ['per_page' => 100];
    $result = uw_wcms_tools_query_gitlab_api('projects/' . $project_id . '/repository/branches', $query);
    if ($result['http_status'] === 200) {
      $cache[$project_id] = $result['body'];
    }
    else {
      $cache[$project_id] = NULL;
    }
  }
  return $cache[$project_id];
}

/**
 * Returns an array of branch names in a project with caching.
 *
 * @param int $project_id
 *   The project ID of the project to search.
 *
 * @return string[]|null
 *   An array of branch names or NULL on error.
 */
function uw_wcms_tools_get_branch_names(int $project_id): ?array {
  $branches = uw_wcms_tools_get_branches($project_id);
  if ($branches) {
    $branch_names = [];
    foreach ($branches as $branch) {
      $branch_names[] = $branch->name;
    }
    return $branch_names;
  }
  return NULL;
}

/**
 * Return a valid semver version of a branch name.
 *
 * @param string $branch
 *   The branch name.
 *
 * @return string|null
 *   If $branch is a valid semver, return it. If it is an "8.x" branch name,
 *   return the semver equivalent, for example, 8.x-2.x becomes 2.0.x.
 *   Otherwise, return NULL.
 */
function uw_wcms_tools_get_semver_branch_name(string $branch): ?string {
  // Already a valid semver branch.
  if (preg_match('/^\d+\.\d+\.x$/', $branch)) {
    return $branch;
  }
  // Convert "8.x" branch name into semver.
  elseif (preg_match('/^8.x-(\d+)\.x$/', $branch, $matches)) {
    return $matches[1] . '.0.x';
  }
  // Invalid branch name.
  return NULL;
}

/**
 * Create semver branches for each 8.x branch and move default if it is 8.x.
 *
 * @param object $project
 *   The project object.
 */
function uw_wcms_tools_create_semver(stdClass $project): void {
  $branches = uw_wcms_tools_get_branch_names($project->id);

  // Create semver for each 8.x, unless it already exists.
  foreach ($branches as $branch_orig) {
    $branch_semver = uw_wcms_tools_get_semver_branch_name($branch_orig);
    if ($branch_semver && $branch_semver !== $branch_orig) {
      echo $branch_orig . ' -> ' . $branch_semver . ': ';
      if (in_array($branch_semver, $branches, TRUE)) {
        echo uw_wcms_tools_shell_color("Already exists.\n", 'blue');
      }
      else {
        echo 'Creating... ';
        $params = [
          'branch' => $branch_semver,
          'ref' => $branch_orig,
        ];
        $created_branch = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/branches', $params, 'POST');
        if ($created_branch['http_status'] === 201) {
          echo uw_wcms_tools_shell_color("Done.\n", 'green');
        }
        else {
          echo uw_wcms_tools_shell_color("Error.\n", 'red');
          var_dump($created_branch);
        }
      }
    }
  }

  // If the default branch is 8.x, set to the corresponding semver.
  $branch_semver = uw_wcms_tools_get_semver_branch_name($project->default_branch);
  if ($branch_semver && $branch_semver !== $project->default_branch) {
    echo 'Update default: ' . $project->default_branch . ' -> ' . $branch_semver . ' ';
    $params = [
      'default_branch' => $branch_semver,
    ];
    $response = uw_wcms_tools_query_gitlab_api('projects/' . $project->id, $params, 'PUT');
    if ($response['http_status'] === 200) {
      echo uw_wcms_tools_shell_color("Done.\n", 'green');
    }
    else {
      echo uw_wcms_tools_shell_color("Error.\n", 'red');
      var_dump($response);
    }
  }
}

/**
 * Return the most recent semver branch.
 *
 * @param int $project_id
 *   The project ID.
 *
 * @return string|null
 *   The branch name or NULL if there are none that match.
 */
function uw_wcms_tools_get_branch_latest(int $project_id): ?string {
  $branches = [];
  foreach (uw_wcms_tools_get_branches($project_id) as $branch) {
    if (preg_match('/^\d+\.\d+\.x$/', $branch->name)) {
      $branches[] = $branch->name;
    }
  }

  return uw_wcms_tools_version_compare_array($branches);
}

/**
 * Return the default branch if semver otherwise the most recent semver branch.
 *
 * @param int $project_id
 *   The project ID of the project to search.
 *
 * @return string|null
 *   The branch name or NULL if there are none that match.
 */
function uw_wcms_tools_get_branch_default_or_latest(int $project_id): ?string {
  $project = uw_wcms_tools_get_project($project_id);

  $branch = uw_wcms_tools_get_semver_branch_name($project->default_branch);
  if (!$branch) {
    $branch = uw_wcms_tools_get_branch_latest($project->id);
  }
  return $branch;
}

/**
 * Return the current development and release branches.
 *
 * The dev branch is the default branch if it is valid, otherwise, the most
 * recent valid branch. The release branch is the corresponding release branch.
 *
 * @param int $project_id
 *   The project ID.
 *
 * @return array|null
 *   NULL if no valid branches, otherwise an array with keys:
 *    - dev: The semver current development branch.
 *    - rel: The semver current release branch.
 *    - tag_latest: The latest tag.
 *    - tag_next: The next tag.
 */
function uw_wcms_tools_get_current_branches(int $project_id): ?array {
  $dev_branch = uw_wcms_tools_get_branch_default_or_latest($project_id);

  // Return early if no semver branch.
  if (!$dev_branch) {
    return NULL;
  }

  $tag_latest = uw_wcms_tools_get_tag_latest($project_id, $dev_branch);
  return [
    'dev' => $dev_branch,
    'rel' => 'prod/' . $dev_branch,
    'tag_latest' => $tag_latest,
    'tag_next' => uw_wcms_tools_next_semver($tag_latest->name ?? $dev_branch),
  ];
}

/**
 * Merge project development to release branch and tag as next patch level.
 *
 * @param string $namespace
 *   The GitLab namespace of the project to act on.
 * @param string $path
 *   The GitLab path of the project to act on.
 */
function uw_wcms_tools_gitlab_tag_release(string $namespace, string $path): void {
  // Load project.
  $project = uw_wcms_tools_get_project($namespace, $path);
  if (!$project) {
    throw new Exception('Invalid project.');
  }
  // Ensure these are not used. Use properties from $project.
  unset($namespace, $path);

  echo uw_wcms_tools_shell_color('Project: ' . $project->path_with_namespace . "\n", 'blue');

  echo "Ensuring this project has semver branches...\n";
  uw_wcms_tools_create_semver($project);

  $current_branches = uw_wcms_tools_get_current_branches($project->id);

  $branch_names = uw_wcms_tools_get_branch_names($project->id);

  // Update or create release branch.
  if (in_array($current_branches['rel'], $branch_names)) {
    echo 'Merge ' . $current_branches['dev'] . ' into ' . $current_branches['rel'] . "...\n";

    $release_branch = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/branches/' . urlencode($current_branches['rel']));
    if ($release_branch['http_status'] !== 200) {
      echo uw_wcms_tools_shell_color("Error.\n", 'red');
      var_dump($release_branch);
      throw new Exception('Unable to load release branch.');
    }

    $dev_branch = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/branches/' . urlencode($current_branches['dev']));
    if ($dev_branch['http_status'] !== 200) {
      echo uw_wcms_tools_shell_color("Error.\n", 'red');
      var_dump($dev_branch);
      throw new Exception('Unable to load development branch.');
    }

    // Create array of commit hashes of release branch tip and its parents.
    $tip_and_parents_of_release = array_merge([$release_branch['body']->commit->id], $release_branch['body']->commit->parent_ids);
    // Test if there is development since the last merge.
    if (in_array($dev_branch['body']->commit->id, $tip_and_parents_of_release)) {
      echo uw_wcms_tools_shell_color("No development. Nothing to tag.\n", 'green');
      // Nothing to tag, so return now.
      return;
    }
    else {
      // Merge changes to development branch into release branch.
      //
      // Path of this project's local repository clone.
      $repo_path = UW_WCMS_TOOLS_REPOSITORY_DIRECTORY . '/' . $project->path;

      // Test if this project has already been cloned. If not, clone it.
      if (!is_dir($repo_path)) {
        if (!chdir(UW_WCMS_TOOLS_REPOSITORY_DIRECTORY)) {
          throw new Exception('Unable to chdir into ' . UW_WCMS_TOOLS_REPOSITORY_DIRECTORY . '.');
        }
        exec('git clone --branch=' . escapeshellarg($current_branches['dev']) . ' ' . escapeshellarg($project->ssh_url_to_repo), $output, $result_code);
        if ($result_code) {
          throw new Exception('Unable to clone; status: ' . $result_code . '.');
        }
      }

      // Merge the development branch into release branch and push.
      if (!chdir($repo_path)) {
        throw new Exception('Unable to chdir into ' . $repo_path . '.');
      }
      // Reset branches and merge.
      $merge_commands = [
        'git fetch',
        'git checkout ' . escapeshellarg($current_branches['dev']),
        'git reset --hard origin/' . escapeshellarg($current_branches['dev']),
        'git checkout ' . escapeshellarg($current_branches['rel']),
        'git reset --hard origin/' . escapeshellarg($current_branches['rel']),
        'git merge --no-ff ' . escapeshellarg($current_branches['dev']),
      ];
      exec(implode(' && ', $merge_commands), $output, $result_code);
      if ($result_code) {
        throw new Exception('Unable to merge; status: ' . $result_code . '.');
      }
      // Push.
      exec('git push origin ' . escapeshellarg($current_branches['rel']), $output, $result_code);
      if ($result_code) {
        throw new Exception('Unable to push; status: ' . $result_code . '.');
      }

      // Update release branch.
      $release_branch = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/branches/' . urlencode($current_branches['rel']));
      if ($release_branch['http_status'] !== 200) {
        echo uw_wcms_tools_shell_color("Error.\n", 'red');
        var_dump($release_branch);
        throw new Exception('Unable to load release branch after merge.');
      }
      echo uw_wcms_tools_shell_color("Merge done.\n", 'green');
    }
  }
  elseif ($current_branches['rel'] && $current_branches['dev']) {
    echo 'Create ' . $current_branches['rel'] . ' from ' . $current_branches['dev'] . '... ';
    $params = [
      'branch' => $current_branches['rel'],
      'ref' => $current_branches['dev'],
    ];
    $release_branch = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/branches', $params, 'POST');
    if ($release_branch['http_status'] === 201) {
      echo uw_wcms_tools_shell_color("Done.\n", 'green');
    }
    else {
      echo uw_wcms_tools_shell_color("Error.\n", 'red');
      var_dump($release_branch);
      throw new Exception('Branch creation failed.');
    }
  }
  else {
    throw new Exception('Project does not have Drupal 8 or later version.');
  }

  // Create tag.
  echo 'Tag ' . $current_branches['rel'] . ' as ' . $current_branches['tag_next'] . '... ';
  // Check that there is not already a tag pointing at the branch tip.
  $params = [
    'type' => 'tag',
  ];
  $release_branch_tip_tags = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/commits/' . $release_branch['body']->commit->id . '/refs', $params);
  if ($release_branch_tip_tags['http_status'] !== 200) {
    echo uw_wcms_tools_shell_color("Error.\n", 'red');
    var_dump($release_branch_tip_tags);
    throw new Exception('Reading tags at branch tip failed.');
  }
  elseif ($release_branch_tip_tags['body']) {
    echo uw_wcms_tools_shell_color("Branch tip already part of a tag. Nothing created.\n", 'yellow');
  }
  else {
    // Do tag creation.
    $params = [
      'tag_name' => $current_branches['tag_next'],
      'ref' => $release_branch['body']->commit->id,
    ];
    $new_tag = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/tags', $params, 'POST');
    if ($new_tag['http_status'] === 201) {
      echo uw_wcms_tools_shell_color("Done.\n", 'green');
    }
    else {
      echo uw_wcms_tools_shell_color("Error.\n", 'red');
      var_dump($new_tag);
      throw new Exception('Tag creation failed.');
    }
  }
}

/**
 * Return an array of WCMS projects in wcms/drupal-recommended-project.
 *
 * @return array[]
 *   An array of project arrays with keys 'namespace' and 'path'.
 */
function uw_wcms_tools_gitlab_get_profile_projects(): array {
  // Get composer.json from development branch in
  // wcms/drupal-recommended-project.
  $project = uw_wcms_tools_get_project('wcms', 'drupal-recommended-project');
  $file_path = 'composer.json';
  $params = [
    'ref' => uw_wcms_tools_get_branch_default_or_latest($project->id),
  ];
  $composer = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/files/' . urlencode($file_path) . '/raw', $params);
  if ($composer['http_status'] !== 200) {
    throw new Exception('Unable to load ' . $file_path . ' from wcms/drupal-recommended-project.');
  }

  // Make an array of all projects in the 'wcms' group.
  $profile_projects = [];
  foreach (['require', 'require-dev'] as $type) {
    if (isset($composer['body']->$type)) {
      foreach (array_keys((array) $composer['body']->$type) as $project) {
        list($project_arr['namespace'], $project_arr['path']) = explode('/', $project, 2);
        if ($project_arr['namespace'] === 'wcms') {
          $profile_projects[] = $project_arr;
        }
      }
    }
  }
  return $profile_projects;
}

/**
 * Get a file from a GitLab repository.
 *
 * @param string $project
 *   The repository to load from.
 * @param string $file_path
 *   The file to load.
 * @param string $ref
 *   The ref of the file to load.
 *
 * @return array
 *   The GitLab API response.
 */
function uw_wcms_tools_gitlab_get_file(string $project, string $file_path, string $ref): array {
  $id = urlencode($project);
  $path = urlencode($file_path);
  $query = [
    'ref' => $ref,
  ];
  $result = uw_wcms_tools_query_gitlab_api('projects/' . $id . '/repository/files/' . $path, $query);

  if (isset($result['body']->content)) {
    $result['body']->content = base64_decode($result['body']->content);
  }

  return $result;
}

/**
 * List commits in a GitLab repository.
 *
 * @param string $project
 *   The repository to load from.
 * @param string $start
 *   The ref to load commits from or the start of the series of commits to load.
 * @param string $end
 *   The end of the series of commits to load.
 *
 * @return array
 *   The GitLab API response.
 */
function uw_wcms_tools_gitlab_get_commits(string $project, string $start, string $end = NULL): array {
  $ref_name = $start;
  if ($end) {
    $ref_name .= '..' . $end;
  }

  $id = urlencode($project);
  $query = [
    'ref_name' => $ref_name,
    'per_page' => 100,
  ];
  return uw_wcms_tools_query_gitlab_api('projects/' . $id . '/repository/commits', $query);
}

/**
 * Add commits between two revisions to an array of projects.
 *
 * @param array $projects
 *   An array of arrays of revisions keyed by project name.
 */
function uw_wcms_tools_gitlab_get_project_commits(array &$projects): void {
  foreach ($projects as $name => $versions) {
    // Exclude non-WCMS projects.
    list($group) = explode('/', $name, 2);
    if ($group !== 'wcms') {
      // Indicate that lookup was not attempted.
      $projects[$name]['commits'] = NULL;
      continue;
    }

    $projects[$name]['commits'] = [];

    // Exclude projects that are not upgrades.
    if (!$versions['start'] || !$versions['end'] || $versions['start'] === $versions['end']) {
      continue;
    }

    $commits = uw_wcms_tools_gitlab_get_commits($name, $versions['start'], $versions['end']);
    if ($commits['http_status'] !== 200) {
      throw new Exception('Unable to load commits.');
    }
    foreach ($commits['body'] as $commit) {
      // @todo Replace with str_starts_with() after upgrade to PHP 8.
      if (strncmp($commit->title, 'Merge branch', 12) === 0) {
        continue;
      }

      $projects[$name]['commits'][] = $commit;
    }
  }
}
