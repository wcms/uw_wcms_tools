<?php

/**
 * @file
 * Main hook implementations and shared functions.
 */

/**
 * Implements hook_menu().
 */
function uw_wcms_tools_menu() {
  $items = array();
  $items['wcms-tools'] = [
    'page callback' => 'drupal_goto',
    // Always allow redirect.
    'access callback' => TRUE,
  ];
  $items['wcms-tools/tickets'] = [
    'title' => 'Create corresponding Jira/RT ticket',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_wcms_tools_ticket_create_form'),
    'access arguments' => array('administer modules'),
    'file' => 'uw_wcms_tools.tickets.inc',
  ];
  $items['wcms-tools/tickets/rt/%'] = [
    'title' => 'View RT ticket',
    'page callback' => 'uw_wcms_tools_rt_api_ticket_view',
    'page arguments' => array(3),
    'access arguments' => array('administer modules'),
    'file' => 'uw_wcms_tools.rt.inc',
  ];
  $items['wcms-tools/contrib-update'] = [
    'title' => 'Create Drupal contrib module update tickets',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_wcms_tools_contrib_module_update_form'),
    'access arguments' => array('administer modules'),
    'file' => 'uw_wcms_tools.drupal.modules.inc',
  ];
  $items['wcms-tools/tags'] = array(
    'title' => 'UW custom modules',
    'page callback' => 'uw_wcms_tools_tags',
    'access arguments' => array('administer modules'),
    'file' => 'uw_wcms_tools.modules.inc',
  );
  $items['wcms-tools/new-tag'] = array(
    'title' => 'New tag',
    'page callback' => 'uw_wcms_tools_new_tag_done',
    'access arguments' => array('administer modules'),
    'file' => 'uw_wcms_tools.modules.inc',
  );
  $items['wcms-tools/new-tag/%uw_wcms_tools_project/%/%'] = array(
    'title' => 'New tag',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_wcms_tools_new_tag_confirm', 2, 3, 4),
    'access arguments' => array('administer modules'),
    'file' => 'uw_wcms_tools.modules.inc',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements template_preprocess_html().
 *
 * Add "wide" class to body.
 */
function uw_wcms_tools_preprocess_html(&$variables) {
  $arg = arg();
  if ($arg[0] === 'wcms-tools') {
    $variables['classes_array'][] = 'wide';
  }
}

/**
 * Loader function for a project object, loading from the API Gitlab API.
 *
 * Wrapper for uw_wcms_tools_get_project().
 *
 * This must be in the same file as uw_wcms_tools_menu() or the Auto-Loader
 * Wildcard ("%uw_wcms_tools_project") will not work.
 *
 * @param int|string $id_or_namespace
 *   The ID of the project to load or its namespace if loading by name.
 * @param string $project_name
 *   The name of the project to load.
 *
 * @return object|null
 *   The project object or NULL if it cannot be found.
 */
function uw_wcms_tools_project_load($id_or_namespace, $project_name = NULL) {
  require_once 'uw_wcms_tools.lib.inc';
  return uw_wcms_tools_get_project($id_or_namespace, $project_name);
}

/**
 * Implements hook_libraries_info().
 */
function uw_wcms_tools_libraries_info() {
  $libraries = [];
  $libraries['RTPHPLib'] = [
    'name' => 'RTPHPLib',
    'vendor url' => 'https://github.com/dersam/RTPHPLib',
    'download url' => 'https://github.com/dersam/RTPHPLib.git',
    'version arguments' => [
      'file' => 'RequestTracker.php',
      'pattern' => '/RTPHPLib v([\d.]+)/',
      'lines' => 5,
    ],
    'files' => [
      'php' => ['RequestTracker.php'],
    ],
  ];
  return $libraries;
}
