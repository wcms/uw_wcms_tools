<?php

/**
 * @file
 * General functions.
 */

require_once 'uw_wcms_tools.gitlab.inc';

/**
 * Return the HTTP status code from a request.
 *
 * @param array $http_response_header
 *   Array of response headers matching the PHP built-in $http_response_header.
 *
 * @return int|null
 *   The HTTP status code or NULL if the reponse does not appear to be HTTP/1.
 */
function _uw_wcms_tools_get_http_status(array $http_response_header) {
  $header = explode(' ', $http_response_header[0]);
  if (substr($header[0], 0, 7) === 'HTTP/1.') {
    return (int) $header[1];
  }
}

/**
 * Return the first or next version number.
 *
 * @param string $current
 *   The current version number.
 *
 * @return string|null
 *   The version one higher than $current, the first version number if $current
 *   is NULL, or NULL if $current is not valid.
 */
function uw_wcms_tools_next_version($current = NULL) {
  $current = (string) $current;
  if (!$current) {
    $core = 7;
    $major = 1;
    $minor = 0;
  }
  elseif (preg_match('/^(\d+)\.x-(\d+)\.(\d+)(?:\.\d+)?(-(?:alpha|beta|rc)\d+)?$/', $current, $matches)) {
    $core = (int) $matches[1];
    $major = (int) $matches[2];
    $minor = (int) $matches[3];

    // Increment minor if version does not end with -alphaN, -betaN, or -rcN.
    if (!isset($matches[4])) {
      $minor++;
    }
  }
  else {
    return;
  }
  return $core . '.x-' . $major . '.' . $minor;
}

/**
 * Return the first or next semantic version number.
 *
 * @param string $version
 *   The current version number or branch.
 *
 * @return string|null
 *   If $version is a semver version, return the next patch leve. If it is a
 *   branch, such as 1.2.x return the first version on that branch. Otherwise,
 *   return NULL.
 */
function uw_wcms_tools_next_semver(string $version): ?string {
  // Version number provided.
  if (preg_match('/^(\d+)\.(\d+)\.(\d+)(-(?:alpha|beta|rc)\d+)?$/', $version, $matches)) {
    $major = (int) $matches[1];
    $minor = (int) $matches[2];
    $patch = (int) $matches[3];

    // Increment minor if version does not end with -alphaN, -betaN, or -rcN.
    if (!isset($matches[4])) {
      $patch++;
    }
  }
  // Branch provided.
  elseif (preg_match('/^(\d+)\.(\d+)\.x$/', $version, $matches)) {
    $major = (int) $matches[1];
    $minor = (int) $matches[2];
    $patch = 0;
  }
  else {
    return NULL;
  }
  return $major . '.' . $minor . '.' . $patch;
}

/**
 * Parse a site makefile.
 *
 * @param string $filename
 *   A filename or URL suitable to be the $filename parameter to fopen().
 *
 * @return array|null
 *   An array with repository paths as keys and versions as values or NULL if
 *   there is no makefile.
 */
function uw_wcms_tools_makefile_parse($filename) {
  $file = @fopen($filename, 'r');
  if (!$file) {
    return;
  }
  $makefile = [];
  while ($line = fgets($file)) {
    // Get the URL path of the repository.
    if (preg_match('/^[^;]*projects\[([^]]+)\]\[download\]\[url\] *= *[\'"]([^"]+)[\'"]/', $line, $backref)) {
      // Clean-up HTTP: https://git.uwaterloo.ca/<group>/<repository>.git
      $path = parse_url($backref[2], PHP_URL_PATH);
      $path = preg_replace(',^/(.+)\.git$,', '$1', $path);
      // Clean-up SSH: gitlab@git.uwaterloo.ca:<group>/<repository>.git.
      $path = preg_replace(',^.+:(.+)\.git$,', '$1', $path);
      $path = preg_replace(',/services_$,', '/services', $path);
      $makefile[$backref[1]]['path'] = $path;
    }
    // Get the tag, revision, and/or branch.
    elseif (preg_match('/^[^;]*projects\[([^]]+)\]\[download\]\[(tag|revision|branch)\] *= *"([^"]+)"/', $line, $backref)) {
      $makefile[$backref[1]]['version'][$backref[2]] = $backref[3];
    }
  }
  fclose($file);

  $return = [];
  while ($entry = array_pop($makefile)) {
    if (isset($entry['path']) && isset($entry['version'])) {
      $return[$entry['path']] = uw_wcms_tools_remove_local_mod_version($entry['version']);
    }
  }
  return $return;
}

/**
 * Parse the latest tag of the profile makefile.
 *
 * Progress messages are output.
 *
 * @return array|null|false
 *   The output of uw_wcms_tools_makefile_parse() for the latest tag of the
 *   profile makefile.
 */
function uw_wcms_tools_get_profile_makefile() {
  $group = 'wcms';
  $project = 'uw_base_profile';

  $profile = uw_wcms_tools_get_project($group, $project);
  $profile_latest_tag = uw_wcms_tools_get_tag_latest($profile->id, NULL, TRUE);
  if (php_sapi_name() === 'cli') {
    echo 'Parsing makefile for ' . $project . ' ' . $profile_latest_tag->name . '...';
  }
  $profile_makefile = uw_wcms_tools_makefile_parse($profile->web_url . '/raw/' . $profile_latest_tag->name . '/' . $project . '.make');
  if (php_sapi_name() === 'cli') {
    echo ' ';
    if (is_array($profile_makefile)) {
      echo 'OK';
    }
    elseif ($profile_makefile === NULL) {
      echo 'No makefile';
    }
    else {
      echo 'Error';
    }
    echo "\n";
  }
  return $profile_makefile;
}

/**
 * Convert a version tag into one with local modification indicators removed.
 *
 * @param string|string[] $tag
 *   The VCS tag or an array of tags.
 *
 * @return string|string[]
 *   The modified tag or array of tags.
 */
function uw_wcms_tools_remove_local_mod_version($tag) {
  return preg_replace('/-uw_(wcms|os)\d+$/', '', $tag);
}

/**
 * Return text with shell coloring.
 *
 * @param string $string
 *   The string to color.
 * @param string $color
 *   The color to make the text.
 *
 * @return string
 *   $string starting with the color code and ending in the reset code.
 */
function uw_wcms_tools_shell_color(string $string, string $color): string {
  static $colors = [
    'black' => 30,
    'red' => 31,
    'green' => 32,
    'yellow' => 33,
    'blue' => 34,
    'purple' => 35,
    'cyan' => 36,
    'white' => 37,
  ];
  static $reset = "\e[0m";

  return "\e[0;" . $colors[$color] . 'm' . $string . $reset;
}
