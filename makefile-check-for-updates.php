#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'devops/uw_devops.inc';
require_once 'uw_wcms_tools.lib.inc';

global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'makefile-check-for-updates.php all|URL-PATH
Checks if modules in site makefiles have been updated or are in use on multiple
sites. Specify a URL-PATH to check one site or use "all".';
min_args($argv, 1);

if ($argv[1] === 'all') {
  $site_makefiles = uw_wcms_tools_get_projects('wcms-sites')['wcms-sites'];
}
else {
  $url_path = $argv[1];
  if (!valid_url_path($url_path)) {
    throw new Exception('Invalid url_path.');
  }
  $project = uw_wcms_tools_get_project('wcms-sites', $url_path);
  if (!$project) {
    throw new Exception('No site repository.');
  }
  $site_makefiles = [$project];
}

try {
  uw_wcms_tools_makefiles_check_versions($site_makefiles);
}
catch (Exception $e) {
  msg($e->getMessage());
}

/**
 * Outputs a version check report for one or more modules.
 *
 * @param object[] $site_makefiles
 *   An array of project objects. Only properties path and default_branch are
 *   used.
 */
function uw_wcms_tools_makefiles_check_versions(array $site_makefiles) {
  $profile_makefile = uw_wcms_tools_get_profile_makefile();

  // Track if modules are used in more than one site makefile.
  $modules_reused = [];
  // Track sites with invalid site makefiles.
  $makefile_error = [];

  foreach ($site_makefiles as $site) {
    try {
      $url_path = repository_path_to_url_path($site->path);
    }
    catch (Exception $e) {
      echo 'Error: ' . $e->getMessage() . "\n";
      echo 'Skipping: ' . $site->path . "\n";
      continue;
    }

    echo 'Site makefile: ' . $site->path . ': ';

    if (!$site->default_branch) {
      echo "Empty, skipping.\n";
      continue;
    }
    // To work with other parts of the WCMS tools, the default branch for site
    // makefiles must be "master".
    elseif ($site->default_branch !== 'master') {
      echo "Default branch not master, skipping.\n";
      continue;
    }

    $makefile = uw_wcms_tools_makefile_parse(site_makefile_url($url_path));
    if ($makefile) {
      foreach ($makefile as $module => &$version) {
        // Track where modules are used to detect modules used in site makefiles
        // of multiple sites.
        $modules_reused[$module][] = $url_path;

        // Get latest tag of this project.
        $latest_tag = NULL;
        list($namespace, $project_name) = explode('/', $module, 2);
        // Module "services" is named "services_" in Gitlab.
        $project_name = ($project_name === 'services') ? 'services_' : $project_name;
        $project = uw_wcms_tools_get_project($namespace, $project_name);
        if (is_object($project)) {
          $filter = isset($version['branch']) ? $version['branch'] : '7.x';
          $latest_tag = uw_wcms_tools_get_tag_latest($project->id, $filter, TRUE);
        }
        $version['latest'] = $latest_tag ? uw_wcms_tools_remove_local_mod_version($latest_tag->name) : 'No version';

        // Determine what to display for version of this module in the makefile.
        if (isset($version['tag'])) {
          $version['version'] = $version['tag'];
        }
        elseif (isset($version['branch']) && isset($version['revision'])) {
          $version['version'] = $version['branch'] . ' / ' . $version['revision'];
        }
        elseif (isset($version['branch'])) {
          $version['version'] = 'Branch: ' . $version['branch'];
        }
        elseif (isset($module_info['revision'])) {
          $version['version'] = 'Revision: ' . $version['revision'];
        }

        // Determine what to display regarding this module in the profile.
        if (isset($profile_makefile[$module]['tag'])) {
          if (isset($version['tag']) && version_compare($profile_makefile[$module]['tag'], $version['tag'], '<')) {
            $version['in_profile'] = 'Older';
          }
          elseif ($profile_makefile[$module]['tag'] === $version['latest']) {
            $version['in_profile'] = 'Latest';
          }
          else {
            $version['in_profile'] = 'Newer';
          }
        }
        else {
          $version['in_profile'] = NULL;
        }

        // If this project is up-to-date and does not appear in the profile or
        // the profile version is older, do not include in output.
        if ($version['version'] === $version['latest'] && (!$version['in_profile'] || $version['in_profile'] === 'Older')) {
          unset($makefile[$module]);
        }
      }

      // If there are still modules in $makefile, output report.
      if ($makefile) {
        ksort($makefile);
        $format = '%-45s %25s %25s %10s';
        echo "\n" . sprintf($format, 'Module', 'Version in makefile', 'Latest version', 'In profile') . "\n";
        foreach ($makefile as $module => $version) {
          echo sprintf($format, $module, $version['version'], $version['latest'], $version['in_profile']) . "\n";
        }
      }
      else {
        echo 'No issues in makefile';
      }
    }
    elseif ($makefile === FALSE) {
      echo 'Makefile error';
      $makefile_error[] = $url_path;
    }
    else {
      echo 'No makefile';
    }
    echo "\n";
  }

  if ($makefile_error) {
    echo "\nMakefile error:\n";
    print_r($makefile_error);
  }

  // Report modules used on more than two sites (this cut-off is approved by
  // kpaxman).
  $reuse_limit = 2;
  $modules_reused_output = '';
  ksort($modules_reused);
  foreach ($modules_reused as $module => $users) {
    if (count($users) > $reuse_limit) {
      $in_profile = isset($profile_makefile[$module]) ? ' (in profile)' : NULL;
      $modules_reused_output .= "\n" . $module . $in_profile . ': ' . implode(', ', $users) . "\n";
    }
  }
  if ($modules_reused_output) {
    echo "\nModules used on more than " . $reuse_limit . " sites:\n" . $modules_reused_output;
  }
}
