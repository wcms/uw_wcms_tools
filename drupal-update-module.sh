# Updates the git.uwaterloo.ca copy of a Drupal module from drupal.org.

# Get directory of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

module=${1}
if [ -z "$CLONE_DIR" ]; then
  clone_dir="/home/wcms-wkr/Repositories/$module"
else
  clone_dir="$CLONE_DIR/$module"
fi

if [ "$module" == "" ]; then
  echo 'You must enter a module machine name.'
  exit 1
elif [ -d $clone_dir ]; then
  cd $clone_dir
  git fetch
else
  if [[ "$module" = "core-recommended" || "$module" = "core-composer-scaffold" || "$module" = "core-project-message" ]]; then
    git clone --mirror https://github.com/drupal/$module.git $clone_dir
  elif [ "$module" = "pantheon_domain_masking" ]; then
    git clone --mirror https://github.com/pantheon-systems/$module.git $clone_dir
  else
    git clone --mirror https://git.drupalcode.org/project/$module.git $clone_dir
  fi
  if [ ! -d $clone_dir ]; then
    echo 'Clone failed.'
    exit 1
  fi
  cd $clone_dir

  # Gitlab cannot handle a repository named "services", so we use "services_".
  module_gitlab=$module;
  if [ "$module" == "services" ]; then
    module_gitlab=${module}_;
  fi

  git remote add gitlab ist-git@git.uwaterloo.ca:drupal-org/$module_gitlab.git
fi
git push gitlab --all
git push gitlab --tags

# Ensure repository matches standards.
$DIR/gitlab-repo-settings.php `git config --get remote.gitlab.url`
cd -

rm -rf $clone_dir
