<?php

/**
 * @file
 * Functions related to the site makefiles database.
 */

/**
 * If the PDO object has an error, output it and exit.
 *
 * @param object $pdo
 *   The PDO object.
 */
function uw_pdo_error($pdo, $line = NULL) {
  if ($pdo->errorCode() !== '00000') {
    if ($line) {
      echo 'Line: ' . $line . ': ';
    }
    var_dump($pdo->errorInfo());
    exit;
  }
}

/**
 * Do a complete update of the site makefiles database. Run by CLI script.
 */
function uw_wcms_tools_makefile_update_module_db() {
  require_once 'devops/uw_devops.inc';
  require_once 'uw_wcms_tools.lib.inc';

  uw_wcms_tools_module_database_update();
  uw_wcms_tools_makefiles_record_profile_use();
  uw_wcms_tools_makefiles_update_versions();
}

/**
 * For each site, record the modules in its makefile in the database.
 */
function uw_wcms_tools_module_database_update() {
  echo "Fetching project list...\n";
  $site_makefiles = uw_wcms_tools_get_projects('wcms-sites')['wcms-sites'];

  $pdo = uw_wcms_tools_site_database_get_connection();

  $sites_with_repos = [];
  foreach ($site_makefiles as $site) {
    try {
      $url_path = repository_path_to_url_path($site->path);
    }
    catch (Exception $e) {
      echo 'Error: ' . $e->getMessage() . '; Skipping: ' . $site->path . "\n";
      continue;
    }

    echo $url_path . ': ';

    $sites_with_repos[] = $url_path;

    if (!$site->default_branch) {
      echo 'Empty, skipping.' . "\n";
      continue;
    }
    // To work with other parts of the WCMS tools, the default branch for site
    // makefiles must be "master".
    elseif ($site->default_branch !== 'master') {
      echo 'Default branch not master, skipping.' . "\n";
      continue;
    }

    $projects = [];
    $pdo->beginTransaction();
    $makefile = uw_wcms_tools_makefile_parse(site_makefile_url($url_path));
    if ($makefile) {
      echo 'OK' . "\n";

      foreach ($makefile as $module => $version) {
        list(, $project) = explode('/', $module, 2);
        // Report branches without revisions.
        if (empty($version['tag']) && empty($version['revision'])) {
          echo 'Branch without revision: ' . $project . "\n";
        }
        // Record version in databse.
        $version = isset($version['tag']) ? $version['tag'] : $version['branch'] . '@' . (isset($version['revision']) ? $version['revision'] : NULL);
        uw_wcms_tools_makefiles_record_site_module_use($url_path, $project, $version);
        $projects[] = $project;
      }
    }
    elseif ($makefile === FALSE) {
      echo 'Makefile error' . "\n";
    }
    else {
      echo 'No makefile' . "\n";
    }

    // Delete entries for projects on this site not in the makefile or all
    // entries if the site has no makefile.
    $values = [
      'site_path' => $url_path,
    ];
    // Keys must begin with a letter.
    foreach ($projects as $key => $value) {
      $values['p' . $key] = $value;
    }
    $sql = 'DELETE FROM site_drupal_project WHERE site_path = :site_path';
    if ($projects) {
      $sql .= ' AND project_id NOT IN (:p' . implode(', :p', array_keys($projects)) . ')';
    }
    $statement = $pdo->prepare($sql);
    $statement->execute($values);
    uw_pdo_error($pdo, __LINE__);
    uw_pdo_error($statement, __LINE__);

    $pdo->commit();
  }

  // Delete entries for sites that do not have repositories. They previously had
  // a repository that has been renamed or deleted.
  $values = [];
  // Keys must begin with a letter.
  foreach ($sites_with_repos as $key => $site_path) {
    $values['p' . $key] = $site_path;
  }
  if ($values) {
    $sql = 'DELETE FROM site_drupal_project WHERE site_path NOT IN (:' . implode(', :', array_keys($values)) . ')';
    $statement = $pdo->prepare($sql);
    $statement->execute($values);
    uw_pdo_error($pdo, __LINE__);
    uw_pdo_error($statement, __LINE__);
  }
}

/**
 * Record the version of a project used by a site.
 *
 * @param string $site_path
 *   The URL path of the site.
 * @param string $project_id
 *   The project ID.
 * @param string $version
 *   The version in use by the site.
 */
function uw_wcms_tools_makefiles_record_site_module_use($site_path, $project_id, $version) {
  $pdo = uw_wcms_tools_site_database_get_connection();

  // UPSERT project name into drupal_project.
  $values = [
    'project_id' => $project_id,
  ];
  $statement = $pdo->prepare('INSERT INTO drupal_project (' . implode(', ', array_keys($values)) . ')
    VALUES (:' . implode(', :', array_keys($values)) . ')
    ON DUPLICATE KEY UPDATE project_id = project_id');
  $statement->execute($values);
  uw_pdo_error($pdo, __LINE__);
  uw_pdo_error($statement, __LINE__);

  // UPSERT project version into site_drupal_project.
  $values = [
    'site_path' => $site_path,
    'project_id' => $project_id,
    'version' => $version,
  ];
  $statement = $pdo->prepare('INSERT INTO site_drupal_project (' . implode(', ', array_keys($values)) . ')
    VALUES (:' . implode(', :', array_keys($values)) . ')
    ON DUPLICATE KEY UPDATE version = :version');
  $statement->execute($values);
  uw_pdo_error($pdo, __LINE__);
  uw_pdo_error($statement, __LINE__);
}

/**
 * Record the versions of all projects used in the profile.
 */
function uw_wcms_tools_makefiles_record_profile_use() {
  $profile_makefile = uw_wcms_tools_get_profile_makefile();
  if (!is_array($profile_makefile)) {
    echo "uw_wcms_tools_makefiles_record_profile_use(): Empty makefile; nothing done.\n";
    return;
  }

  $pdo = uw_wcms_tools_site_database_get_connection();
  $pdo->beginTransaction();

  // Set all version_in_profile_7 to NULL.
  $statement = $pdo->prepare('UPDATE drupal_project SET version_in_profile_7 = NULL');
  $statement->execute();
  uw_pdo_error($pdo, __LINE__);
  uw_pdo_error($statement, __LINE__);

  // For each project, set that version of it that is in the profile.
  $values = [
    'project_id' => NULL,
    'version_in_profile_7' => NULL,
  ];
  $statement = $pdo->prepare('INSERT INTO drupal_project (' . implode(', ', array_keys($values)) . ')
    VALUES (:' . implode(', :', array_keys($values)) . ')
    ON DUPLICATE KEY UPDATE version_in_profile_7 = :version_in_profile_7');
  foreach ($profile_makefile as $project_id => $version) {
    list(, $project_id) = explode('/', $project_id, 2);
    $version = isset($version['tag']) ? $version['tag'] : $version['branch'] . '@' . (isset($version['revision']) ? $version['revision'] : NULL);

    $values = [
      'project_id' => $project_id,
      'version_in_profile_7' => $version,
    ];
    $statement->execute($values);
    uw_pdo_error($pdo, __LINE__);
    uw_pdo_error($statement, __LINE__);
  }

  $pdo->commit();
}

/**
 * Update the lastest version of each project in drupal_project.
 */
function uw_wcms_tools_makefiles_update_versions() {
  $pdo = uw_wcms_tools_site_database_get_connection();

  echo "Updating latest versions for projects...\n";
  $projects = $pdo->query('SELECT project_id FROM drupal_project', PDO::FETCH_ASSOC);

  $project_ids = uw_wcms_tools_get_project_ids([
    'drupal-org',
    'wcms',
    'wcms-decoupled',
    'wcms-openscholar',
    'feds-site',
    'mur-dev',
    'MSI',
    'iqc',
    'library',
    'j4truong',
    'vsivan',
  ]);

  $values = [
    'project_id' => NULL,
    'version_latest_7' => NULL,
    'version_latest_8' => NULL,
  ];
  $statement = $pdo->prepare('INSERT INTO drupal_project (' . implode(', ', array_keys($values)) . ')
    VALUES (:' . implode(', :', array_keys($values)) . ')
    ON DUPLICATE KEY UPDATE
      version_latest_7 = :version_latest_7,
      committer_latest_7 = :committer_latest_7,
      version_latest_8 = :version_latest_8,
      committer_latest_8 = :committer_latest_8');
  $pdo->beginTransaction();
  foreach ($projects as $project_id) {
    $project_id = $project_id['project_id'];

    // GitLab requires "services" to be "services_".
    $project_id_gitlab = $project_id;
    $project_id_gitlab .= ($project_id_gitlab === 'services') ? '_' : NULL;

    if (!isset($project_ids[$project_id_gitlab])) {
      echo $project_id . ": Project missing\n";
      continue;
    }

    $version_7 = uw_wcms_tools_get_tag_latest($project_ids[$project_id_gitlab], 7);
    $committer_latest_7 = isset($version_7->commit->committer_email) ? $version_7->commit->committer_email : NULL;
    $version_7 = isset($version_7->name) ? $version_7->name : NULL;

    $version_8 = uw_wcms_tools_get_tag_latest($project_ids[$project_id_gitlab], 8);
    $committer_latest_8 = isset($version_8->commit->committer_email) ? $version_8->commit->committer_email : NULL;
    $version_8 = isset($version_8->name) ? $version_8->name : NULL;

    $values = [
      'project_id' => $project_id,
      'version_latest_7' => $version_7,
      'committer_latest_7' => $committer_latest_7,
      'version_latest_8' => $version_8,
      'committer_latest_8' => $committer_latest_8,
    ];
    $statement->execute($values);
    uw_pdo_error($pdo, __LINE__);
    uw_pdo_error($statement, __LINE__);
  }
  $pdo->commit();
}

/**
 * Return a connection object for the site database.
 *
 * @return object
 *   The database connection object.
 */
function uw_wcms_tools_site_database_get_connection() {
  static $pdo;
  if (isset($pdo)) {
    return $pdo;
  }

  // This file is on wms-aux1.
  $my_cnf = '/home/wcmsadmin/.my.cnf';
  $db_config = parse_ini_file($my_cnf);

  $db_config['driver'] = 'mysql';
  $db_config['host'] = 'wms-stg-db.private.uwaterloo.ca';
  $db_config['database'] = 'extrasitedatabase';

  $dsn = [
    'dbname' => $db_config['database'],
    'host' => $db_config['host'],
  ];
  $dsn = $db_config['driver'] . ':' . http_build_query($dsn, NULL, ';');
  $pdo = new PDO($dsn, $db_config['user'], $db_config['password']);

  return $pdo;
}
