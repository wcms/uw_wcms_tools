#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'devops/uw_devops.inc';
require_once 'uw_wcms_tools.lib.inc';

global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'makefile-update-repos.php all|URL-PATH
Updates our local repositories of drupal.org modules used in site makefiles.';
min_args($argv, 1);

if ($argv[1] === 'all') {
  echo "Loading site makefiles...\n";
  $site_makefiles = uw_wcms_tools_get_projects('wcms-sites')['wcms-sites'];
}
else {
  $url_path = $argv[1];
  if (!valid_url_path($url_path)) {
    throw new Exception('Invalid url_path.');
  }
  $site_makefiles = [
    new StdClass(),
  ];
  $site_makefiles[0]->default_branch = 'master';
  $site_makefiles[0]->path = $url_path;
}

try {
  uw_wcms_tools_makefiles_update_repos($site_makefiles);
}
catch (Exception $e) {
  msg($e->getMessage());
}

/**
 * Outputs a version check report for one or more modules.
 *
 * @param object[] $site_makefiles
 *   An array of project objects. Only properties path and default_branch are
 *   used.
 */
function uw_wcms_tools_makefiles_update_repos(array $site_makefiles) {
  $modules = [];
  foreach ($site_makefiles as $site) {
    try {
      $url_path = repository_path_to_url_path($site->path);
    }
    catch (Exception $e) {
      echo 'Error: ' . $e->getMessage() . "\n";
      echo 'Skipping: ' . $site->path . "\n";
      continue;
    }

    echo 'Loading site makefile: ' . $site->path . '...';

    $makefile = uw_wcms_tools_makefile_parse(site_makefile_url($url_path));
    if ($makefile) {
      foreach (array_keys($makefile) as $module) {
        $module = explode('/', $module, 2);
        if ($module[0] === 'drupal-org') {
          $modules[$module[1]] = TRUE;
        }
      }
    }
    echo " Done.\n";
  }

  echo "Updating repositories...\n";
  foreach (array_keys($modules) as $module) {
    echo "\nUpdating " . $module . "...\n";
    exec('drupal-update-module.sh ' . escapeshellarg($module));
  }
}
