#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 *
 * This run daily by cron of user wcmsadmi@wms-aux1.
 */

require_once 'devops/uw_devops.inc';
require_once 'uw_wcms_tools.lib.inc';
require_once 'uw_wcms_tools.makefiles.inc';

global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'makefile-update-module-db.php all
Updates the site module database.';
min_args($argv, 1);

uw_wcms_tools_makefile_update_module_db();
