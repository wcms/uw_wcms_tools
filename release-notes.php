#!/usr/bin/env php
<?php

/**
 * @file
 * Generate release notes for the WCMS.
 */

require_once 'devops/uw_devops.inc';
require_once 'uw_wcms_tools.gitlab.inc';
require_once 'uw_wcms_tools.jira.inc';
require_once 'uw_wcms_tools.releasenotes.inc';

global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'release-notes.php START-TAG END-TAG
Generate release notes for the WCMS.';
min_args($argv, 2);

echo uw_wcms_tools_release_notes_generate($argv[1], $argv[2]);
