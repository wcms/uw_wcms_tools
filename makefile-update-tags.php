#!/usr/bin/env php
<?php

/**
 * @file
 * This is a CLI script which updates the makefile in the current directory.
 *
 * Any entries pointing to the master branch of a WCMS project will instead
 * point to the latest tag.
 */

// Exit if not CLI.
if (php_sapi_name() !== 'cli') {
  header('Content-Type: text/plain');
  echo "This can only be run from the CLI.\n";
  exit(1);
}

// Exit if GITLAB_PRIVATE_TOKEN not set. Use the token you can get from
// <gitlab server>/profile/account.
if (!getenv('GITLAB_PRIVATE_TOKEN')) {
  echo "You must set the environment variable GITLAB_PRIVATE_TOKEN.\n";
  exit(1);
}

// Name of the makefile.
define('MAKEFILE', 'uw_base_profile.make');

// Exit if currect directory is not a clone of uw_base_profile.
if (!is_dir('.git') || !is_file(MAKEFILE)) {
  echo "This must be run in a clone of uw_base_profile.\n";
  exit(1);
}

echo 'Updating ' . MAKEFILE . "...\n";

require_once 'uw_wcms_tools.lib.inc';

$project_ids = uw_wcms_tools_get_project_ids(['wcms', 'wcms-decoupled']);

// Go through the makefile, updating each line that contains a reference to a
// master branch.
$makefile = file(MAKEFILE);
foreach ($makefile as &$line) {
  if (preg_match('/projects\[([a-z0-9_-]+)\]\[download\]\[branch\] = "[^"]+"/', $line, $matches)) {
    $module = $matches[1];
    // Only update modules that are WCMS-related.
    if (empty($project_ids[$module])) {
      continue;
    }
    $latest_tag = uw_wcms_tools_get_tag_latest($project_ids[$module]);
    if ($latest_tag) {
      $line = 'projects[' . $module . '][download][tag] = "' . $latest_tag->name . "\"\n";
    }
    else {
      echo "Warning: No tag, left on branch: $module.\n";
    }
  }
}
file_put_contents(MAKEFILE, $makefile);

echo "Done.\n";
