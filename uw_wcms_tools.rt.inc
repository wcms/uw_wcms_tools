<?php

/**
 * @file
 * Functions related to the RT API.
 *
 * @see https://rt-wiki.bestpractical.com/wiki/REST
 */

define('UW_WCMS_TOOLS_RT_SERVER', 'https://rt.uwaterloo.ca');
define('UW_WCMS_TOOLS_RT_URL_PREFIX', UW_WCMS_TOOLS_RT_SERVER . '/Ticket/Display.html?id=');

/**
 * Return a RequestTracker object for rt.uwaterloo.ca.
 *
 * @return RequestTracker
 *   The RequestTracker object.
 */
function uw_wcms_tools_rt_api_get_connection() {
  static $requestTracker;
  if ($requestTracker) {
    return $requestTracker;
  }

  $user = variable_get('rt_username');
  $pass = variable_get('rt_password');
  if (!$user || !$pass) {
    throw new Exception('RT API credentials not configured.');
  }

  $library = libraries_load('RTPHPLib');
  if (empty($library['loaded'])) {
    throw new Exception('RTPHPLib library not available.');
  }

  $requestTracker = new requestTracker(UW_WCMS_TOOLS_RT_SERVER, $user, $pass);
  return $requestTracker;
}

/**
 * Create an RT ticket.
 *
 * @param array $content
 *   The values for the fields. See below for example. Other fields available:
 *   Cc, AdminCc, Owner, Status, Priority, InitialPriority, FinalPriority,
 *   TimeEstimated, Starts, Due, Text.
 *
 * @code
 *   $content = [
 *     'Queue' => 'IST-WCMS-dev',
 *     'Requestor' => 'lkmorlan',
 *     'Subject' => 'Update to ' . $module . ' ' . $version,
 *     'CF.{Category}' => 'maintenance',
 *     'CF.{Related Modules}' => $module,
 *   ];
 * @endcode
 *
 * @return int|null
 *   The ID of the RT ticket or NULL if no ticket was created.
 */
function uw_wcms_tools_rt_api_ticket_create(array $content) {
  $rt = uw_wcms_tools_rt_api_get_connection();
  $result = $rt->createTicket($content);

  // Determine the ID of the RT ticket created.
  foreach (array_keys($result) as $line) {
    if (preg_match('/^# Ticket (\d+) created/', $line, $matches)) {
      return (int) $matches[1];
    }
  }
}

/**
 * Page callback to view an RT ticket.
 */
function uw_wcms_tools_rt_api_ticket_view($ticket) {
  $rt = uw_wcms_tools_rt_api_get_connection();
  // @codingStandardsIgnoreLine
  dpm($rt->getTicketProperties((int) $ticket), 'Ticket');
  // @codingStandardsIgnoreLine
  dpm(uw_wcms_tools_rt_api_get_ticket_history_node((int) $ticket), 'First history node');
  return '';
}

/**
 * Returns the first history node for a ticket.
 *
 * @param int $id
 *   The ID of the RT ticket.
 *
 * @return array|null
 *   The first history node for the ticket or NULL if there is none.
 */
function uw_wcms_tools_rt_api_get_ticket_history_node($id) {
  $id = (int) $id;
  $rt = uw_wcms_tools_rt_api_get_connection();
  $history = $rt->getTicketHistory($id, FALSE);
  foreach (array_keys($history) as $key) {
    if (is_int($key)) {
      $node = $rt->getTicketHistoryNode($id, $key);
      if ($node['Content'] === 'This transaction appears to have no content') {
        $node['Content'] = NULL;
      }
      elseif ($node['Content']) {
        $node['Content'] = trim($node['Content']);
      }
      return $node;
    }
  }
}

/**
 * Create an RT ticket for a Jira ticket.
 *
 * @param int $id
 *   The ID of the Jira ticket.
 *
 * @return int
 *   The ID of the RT ticket.
 */
function uw_wcms_tools_create_rt_for_jira($id) {
  require_once 'uw_wcms_tools.jira.inc';
  $jira_ticket = uw_wcms_tools_jira_api_query('issue/' . $id);

  if (empty($jira_ticket->fields->summary)) {
    throw new Exception('Jira ticket has no subject.');
  }

  $content = [
    'Queue' => 'IST-WCMS-dev',
    'Requestor' => $jira_ticket->fields->reporter->emailAddress,
    'Subject' => $jira_ticket->fields->summary,
    'Text' => $jira_ticket->fields->description,
  ];

  // Create the RT ticket.
  $id = uw_wcms_tools_rt_api_ticket_create($content);

  // Link the RT ticket to the Jira ticket.
  $rt = uw_wcms_tools_rt_api_get_connection();
  $rt->addTicketLink($id, 'ReferredToBy', UW_WCMS_TOOLS_JIRA_URL_PREFIX . $jira_ticket->key);

  // Link the Jira ticket to the RT ticket.
  $data = (object) [
    'fields' => [
      'customfield_10300' => UW_WCMS_TOOLS_RT_URL_PREFIX . $id,
    ],
  ];
  uw_wcms_tools_jira_api_query('issue/' . $jira_ticket->key, $data, 'PUT');

  return $id;
}
