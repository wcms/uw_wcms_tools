<?php

/**
 * @file
 * Functions related to Drupal modules.
 */

/**
 * Form callback to create Drupal contrib module update tickets.
 */
function uw_wcms_tools_contrib_module_update_form(array $form, array &$form_state) {
  $form = [];
  $form['url'] = [
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'URL of module release node on drupal.org',
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Generate tickets',
  ];
  $form['module'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  $form['version'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  return $form;
}

/**
 * Form validation callback.
 */
function uw_wcms_tools_contrib_module_update_form_validate(array $form, array &$form_state) {
  if (preg_match(',https://www.drupal.org/project/([^/]+)/releases/(.+),', $form_state['values']['url'], $matches)) {
    $form_state['values']['module'] = $matches[1];
    $form_state['values']['version'] = $matches[2];
  }
  else {
    form_set_error('url', t('Invalid module update URL.'));
  }
}

/**
 * Form submit callback.
 */
function uw_wcms_tools_contrib_module_update_form_submit(array $form, array &$form_state) {
  $module = $form_state['values']['module'];
  $version = $form_state['values']['version'];
  $depended_on_by = variable_get('wcms_next_release_rt');

  $id = uw_wcms_tools_create_module_update_tickets($module, $version, $depended_on_by);

  require_once 'uw_wcms_tools.rt.inc';
  $args = [
    '@module' => $module,
    '@version' => $version,
    '!url' => l(t('RT#@rt', ['@rt' => $id]), UW_WCMS_TOOLS_RT_URL_PREFIX . $id),
  ];
  drupal_set_message(t('Created !url to update to @module @version.', $args));
}

/**
 * Create an RT for a single module update.
 *
 * @param string $module
 *   The machine-readable name of the module.
 * @param string $version
 *   The version number of the module update.
 * @param int $depended_on_by
 *   The number of an RT ticket to made depend on the new RT ticket.
 * @param string $url
 *   The URL of the module release node. If not set, the default will be used.
 *
 * @todo Have it check for it being a security update and add that to title.
 *
 * @return int
 *   The ID of the RT ticket created.
 */
function uw_wcms_tools_create_module_update_tickets($module, $version, $depended_on_by, $url = NULL) {
  // Create the RT ticket.
  $content = [
    'Queue' => 'IST-WCMS-dev',
    'Requestor' => 'lkmorlan',
    'Subject' => 'Update to ' . $module . ' ' . $version,
    'CF.{Category}' => 'maintenance',
    'CF.{Related Modules}' => $module,
  ];
  require_once 'uw_wcms_tools.rt.inc';
  $id = uw_wcms_tools_rt_api_ticket_create($content);

  // Create the Jira ticket.
  if ($id) {
    // Add links to RT ticket.
    $links = [
      'RefersTo' => $url ?: 'https://www.drupal.org/project/' . $module . '/releases/' . $version,
    ];

    $depended_on_by = (int) $depended_on_by;
    if ($depended_on_by) {
      $links['DependedOnBy'] = 'fsck.com-rt://uwaterloo.ca/ticket/' . $depended_on_by;
    }

    require_once 'uw_wcms_tools.jira.inc';
    $jira_ticket = uw_wcms_tools_create_jira_for_rt($id, $links);

    if (!isset($jira_ticket->key)) {
      drupal_set_message(t('Failed to create corresponding Jira ticket.'), 'error');
    }
  }
  else {
    throw new Exception('Failed to create RT ticket.');
  }

  return $id;
}
