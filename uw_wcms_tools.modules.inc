<?php

/**
 * @file
 * Page for managing module tagging.
 *
 * Page contains list of modules, tags, revisions, and tagging links.
 */

/**
 * Page callback.
 *
 * @return array
 *   Drupal render array.
 */
function uw_wcms_tools_tags() {
  module_load_include('inc', 'uw_wcms_tools', 'uw_wcms_tools.lib');

  // Make single array of all projects.
  $projects = array();
  foreach (uw_wcms_tools_get_projects(array('wcms', 'wcms-decoupled')) as $namespace) {
    foreach ($namespace as $project) {
      $projects[] = $project;
    }
  }

  $makefile_modules = uw_wcms_tools_makefile_get_modules();
  $rows = array();
  foreach ($projects as $project) {
    $tag = uw_wcms_tools_get_tag_latest($project->id, $project->default_branch);
    $commits = uw_wcms_tools_get_commits($project->id);

    $commits_ahead = 0;
    $tag_is_on_default_branch = FALSE;
    $commits_ahead_message = NULL;
    if (isset($tag)) {
      // If the current commit is the same as the tag or the tag's parent,
      // break. Tags from SVN may be a single-commit branch with the only change
      // from the branch being adding the version number.
      $test_commits = $tag->commit->parent_ids;
      $test_commits[] = $tag->commit->id;
      foreach ($commits as $commit) {
        if (in_array($commit->id, $test_commits, TRUE)) {
          $tag_is_on_default_branch = TRUE;
          break;
        }
        $commits_ahead++;
      }
      // If there is a tag, but it is not on the default branch, say so.
      if (!$tag_is_on_default_branch) {
        $commits_ahead_message = t('Tag not on default branch');
      }
    }
    else {
      $commits_ahead = count($commits);
    }
    if (empty($commits_ahead_message)) {
      $commits_ahead_message = $commits_ahead;
    }

    if (isset($tag)) {
      $commits_ahead_link = '/compare/' . $tag->name . '...' . $project->default_branch;
    }
    else {
      $commits_ahead_link = '/commits/' . $project->default_branch;
    }

    // If there are commits to be tagged or the tag is a -alpha, -beta, or -rc,
    // make a tagging link.
    $tag_link = NULL;
    $next_version = uw_wcms_tools_next_version(isset($tag) ? $tag->name : NULL);
    if ($next_version && ($commits_ahead > 0 || (isset($tag) && substr_count($tag->name, '-') > 1))) {
      $tag_link = l(t('Tag as !version', array('!version' => $next_version)), 'wcms-tools/new-tag/' . $project->id . '/' . $next_version . '/' . $commits[0]->id, array('query' => array('destination' => 'wcms-tools/new-tag')));
    }

    $url = uw_wcms_tools_get_project_url($project);
    $row = array(
      l($project->name, $url) . ' (' . l(t('network'), $url . '/network/' . $project->default_branch) . ')',
      isset($tag) ? l($tag->name, $url . '/commits/' . $tag->name) : 'None',
      l($commits_ahead_message, $url . $commits_ahead_link),
      $tag_link,
    );
    // If the module is in the makefile and has commits past the last tag,
    // highlight it.
    if ($makefile_modules && in_array($project->path, $makefile_modules) && $commits_ahead > 0) {
      $row = array(
        'data' => $row,
        'class' => array('upgrade'),
      );
    }
    $rows[] = $row;
  }

  $page = array();
  $page['intro'] = array(
    '#markup' => t('<p>This table shows the name of the module, the latest tag, the number of commits after that tag on the default branch with diff link, and a link to tag the tip of the default branch, if applicable. Highlighted rows are modules that are in the default branch of the makefile and have commits on the default branch after the latest tag. If the latest tag is not on the default branch, there is a message instead of the number of commits ahead.</p>'),
  );
  $page['table'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Module'),
      t('Latest tag'),
      t('Default branch commits ahead of tag'),
      t('Tag default branch'),
    ),
    '#attributes' => array(
      'class' => array('custom-modules'),
    ),
    '#rows' => $rows,
  );
  return $page;
}

/**
 * Display confirmation page for creating a tag.
 *
 * @param array $form
 *   The Drupal form array.
 * @param array $form_state
 *   The Drupal form state array.
 * @param object $project
 *   The project object for the project in which to create the new tag.
 * @param string $tag_name
 *   The name of the tag to create.
 * @param string $commit_id
 *   The ID of the revision to tag.
 *
 * @return array
 *   Output of confirm_form().
 */
function uw_wcms_tools_new_tag_confirm(array $form, array &$form_state, $project, $tag_name, $commit_id) {
  $form['project'] = array(
    '#type' => 'value',
    '#value' => $project->id,
  );
  $form['tag_name'] = array(
    '#type' => 'value',
    '#value' => $tag_name,
  );
  $form['commit_id'] = array(
    '#type' => 'value',
    '#value' => $commit_id,
  );
  $url = uw_wcms_tools_get_project_url($project);
  $commit_id_short = drupal_substr($commit_id, 0, 7);
  return confirm_form($form,
    t('Are you sure you want to tag !project?', array('!project' => $project->name_with_namespace)),
    isset($_GET['destination']) ? $_GET['destination'] : $_GET['q'],
    t('This will create !tag_name from !commit_id.', array(
      '!tag_name' => $tag_name,
      '!commit_id' => l($commit_id_short, $url . '/commits/' . $commit_id_short),
    )),
    t('Tag'),
    t('Cancel')
  );
}

/**
 * Submit handler for creating a tag.
 *
 * @param array $form
 *   The Drupal form array.
 * @param array $form_state
 *   The Drupal form state array.
 */
function uw_wcms_tools_new_tag_confirm_submit(array $form, array &$form_state) {
  $project = uw_wcms_tools_get_project($form['project']['#value']);

  $last_commit = uw_wcms_tools_get_commits($form['project']['#value'], 1);
  if ($last_commit->id === $form['commit_id']['#value']) {
    $result = uw_wcms_tools_create_tag($project->id, $last_commit->id, $form['tag_name']['#value']);
    if ($result['http_status'] === 201) {
      $args = [
        '%project' => $project->name_with_namespace,
        '%tag_name' => $form['tag_name']['#value'],
      ];
      drupal_set_message(t('Default branch of project %project tagged %tag_name.', $args), 'status');
    }
    else {
      if (function_exists('dpm')) {
        // @codingStandardsIgnoreLine
        dpm($result, 'Error object', 'error');
      }
      $args = [
        '%project' => $project->name_with_namespace,
        '%tag_name' => $form['tag_name']['#value'],
      ];
      drupal_set_message(t('Failed to create tag %tag_name in %project.', $args), 'error');
    }
  }
  else {
    drupal_set_message(t('Repository has had additional commits. No tag made.'), 'error');
  }
}

/**
 * Page callback for new tag completion page.
 *
 * @return string
 *   HTML link to the tagging page.
 */
function uw_wcms_tools_new_tag_done() {
  return l(t('Return to UW custom modules list.'), 'wcms-tools/tags');
}

/**
 * Return a list of the modules in the makefile.
 *
 * @return array|null
 *   Array of all modules in the makefile or NULL on error.
 */
function uw_wcms_tools_makefile_get_modules() {
  $project = uw_wcms_tools_get_project('wcms', 'uw_base_profile');
  if (!isset($project->default_branch)) {
    return;
  }
  $makefile = file('https://git.uwaterloo.ca/wcms/uw_base_profile/raw/' . $project->default_branch . '/uw_base_profile.make');
  if (!is_array($makefile)) {
    return;
  }
  $modules = array();
  foreach ($makefile as $line) {
    if (preg_match(',^projects\[.+https://git\.uwaterloo\.ca/[a-z0-9_.-]+/([a-z0-9_.-]+)\.git,', $line, $matches)) {
      $modules[] = $matches[1];
    }
  }
  return $modules;
}
