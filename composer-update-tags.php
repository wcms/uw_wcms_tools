#!/usr/bin/env php
<?php

/**
 * @file
 * This is a CLI script which updates composer.json in the current directory.
 *
 * Any entries pointing to a development branch of a WCMS project will instead
 * point to the latest tag.
 */

// Exit if not CLI.
if (php_sapi_name() !== 'cli') {
  header('Content-Type: text/plain');
  echo "This can only be run from the CLI.\n";
  exit(1);
}

// Exit if currect directory is not a clone of uw_base_profile.
if (!is_dir('.git') || !is_file('composer.json')) {
  echo "This must be run in a clone of uw_base_profile for WCMS 3.\n";
  exit(1);
}

require_once 'uw_wcms_tools.lib.inc';

echo "Updating composer.json...\n";

// Go through the composer.json, updating each line that contains a reference to
// a development branch.
$composer_file = file('composer.json');
foreach ($composer_file as &$line) {
  // Only act on modules in this group.
  $group = 'wcms';
  // Match line from composer.json. Example: "wcms/uw_cfg_common": "dev-1.0.x",
  // Match line whether version is set to a tag or branch with "dev-" prefix.
  // Skip if line does not match.
  if (!preg_match('/(\s*"' . $group . '\/)([a-z0-9_]+)(": ")(dev-)?([0-9.x]+)(",?\s*)/', $line, $matches)) {
    continue;
  }
  $module = $matches[2];
  $branch = $matches[5];

  $latest_tag = uw_wcms_tools_get_tag_latest($group . '/' . $module, $branch);
  if ($latest_tag) {
    $new_line = $matches[1] . $module . $matches[3] . $latest_tag->name . $matches[6];
    echo $module . ($line === $new_line ? ' already at ' : ' setting to ') . $latest_tag->name . "\n";
    $line = $new_line;
  }
  else {
    echo uw_wcms_tools_shell_color('Warning: No tag, left on branch: ' . $module . ".\n", 'red');
  }
}
file_put_contents('composer.json', $composer_file);

echo uw_wcms_tools_shell_color("Done.\n", 'green');
