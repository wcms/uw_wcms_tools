<?php

/**
 * @file
 * Functions related to tickets.
 */

/**
 * Form callback to create a Jira ticket from an RT ticket.
 */
function uw_wcms_tools_ticket_create_form(array $form, array &$form_state) {
  $form = [];
  $form['url'] = [
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'Ticket URL or ID',
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Create ticket',
  ];
  $form['ticket_id'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  $form['ticket_type'] = array(
    '#type' => 'value',
    '#value' => NULL,
  );
  return $form;
}

/**
 * Form validation callback.
 */
function uw_wcms_tools_ticket_create_form_validate(array $form, array &$form_state) {
  if (preg_match(',https://rt\.uwaterloo\.ca/Ticket/Display\.html\?id=(\d+),', $form_state['values']['url'], $matches)) {
    $id = (int) $matches[1];
    $type = 'RT';
  }
  elseif (preg_match(',[[:alnum:]]+-\d+,', $form_state['values']['url'], $matches)) {
    $id = $matches[0];
    $type = 'Jira';
  }
  else {
    $id = (int) $form_state['values']['url'];
    $type = 'RT';
  }
  if (!$id) {
    form_set_error('url', t('Invalid ticket ID.'));
  }
  $form_state['values']['ticket_id'] = $id;
  $form_state['values']['ticket_type'] = $type;
}

/**
 * Form submit callback.
 */
function uw_wcms_tools_ticket_create_form_submit(array $form, array &$form_state) {
  require_once 'uw_wcms_tools.jira.inc';
  require_once 'uw_wcms_tools.rt.inc';

  $ticket_id = $form_state['values']['ticket_id'];

  switch ($form_state['values']['ticket_type']) {
    case 'Jira':
      $copy_id = uw_wcms_tools_create_rt_for_jira($ticket_id);
      break;

    case 'RT':
      $jira = uw_wcms_tools_create_jira_for_rt($ticket_id);
      $copy_id = $jira->key ?? NULL;
      break;
  }

  $args = [
    '!copy' => uw_wcms_tools_get_ticket_link($copy_id),
    '!original' => uw_wcms_tools_get_ticket_link($ticket_id),
  ];
  if ($copy_id) {
    drupal_set_message(t('Created !copy for !original.', $args));
  }
  else {
    drupal_set_message(t('Failed to create corresponding ticket for !original.', $args), 'error');
  }
}

/**
 * Return a link to a ticket.
 *
 * @param int|string $id
 *   The ID of the ticket. When an int, an RT; otherwise Jira.
 *
 * @return string
 *   The link to the ticket.
 */
function uw_wcms_tools_get_ticket_link($id) {
  if (is_int($id)) {
    return l(t('RT#@rt', ['@rt' => $id]), UW_WCMS_TOOLS_RT_URL_PREFIX . $id);
  }
  else {
    return l($id, UW_WCMS_TOOLS_JIRA_URL_PREFIX . $id);
  }
}
