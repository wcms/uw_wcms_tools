#!/usr/bin/env php
<?php

/**
 * @file
 * Normlizes the .info file in the current directory.
 *
 * Only acts if the current directory is a git repository.
 */

$dir = getcwd();
$project = substr(strrchr($dir, DIRECTORY_SEPARATOR), 1);
$info_filename = $dir . DIRECTORY_SEPARATOR . $project . '.info';

if (is_file($info_filename) && is_dir($dir . DIRECTORY_SEPARATOR . '.git')) {
  $info = array();
  foreach (file($info_filename) as $line) {
    // Normalize line.
    $line = preg_replace('/^(.+?) ?= ?"?(.+?)"?$/', '\1 = \2', trim($line));
    // Extract property name.
    $property = preg_replace('/^([A-Za-z_ ]+)[\[ ].*$/', '$1', $line);

    if (preg_match("/^dependencies\[\] = {$project}$/", $line)) {
      // Remove self-dependencies.
    }
    elseif ($property === 'php' && version_compare(preg_replace('/.*?(\d+\.\d+(\.\d+)?).*/', '$1', $line), '5.3', '<')) {
      // Remove obsolete versions.
    }
    else {
      $info[$property][] = $line;
    }
  }

  $allowed_properties = array();
  $allowed_properties['module'] = array(
    'name',
    'description',
    'core',
    'package',
    'dependencies',
    'test_dependencies',
    'stylesheets',
    'scripts',
    'files',
    'php',
    'configure',
    'features',
    'features_exclude',
    'required',
    'hidden',
  );
  $allowed_properties['theme'] = array(
    'name',
    'description',
    'screenshot',
    'core',
    'base theme',
    'regions',
    'features',
    'theme settings',
    'stylesheets',
    'scripts',
    'php',
  );

  // Assume this is a module info file unless the file contains theme-only
  // properties.
  $info_file_type = 'module';
  foreach (array_keys($info) as $key) {
    if (in_array($key, $allowed_properties['theme']) && !in_array($key, $allowed_properties['module'])) {
      $info_file_type = 'theme';
      echo "Theme info file detected because of $key property.\n";
      break;
    }
  }

  $output = array();
  foreach ($allowed_properties[$info_file_type] as $property) {
    if (isset($info[$property])) {
      foreach ($info[$property] as $line) {
        $output[] = trim($line);
      }
    }
  }

  $output = implode("\n", $output) . "\n";
  if ($output !== file_get_contents($info_filename)) {
    file_put_contents($info_filename, $output);
    echo 'Fixed.';
  }
  else {
    echo 'No changes made.';
  }

}
else {
  echo 'No info file or not git repo.';
}

echo "\n";
