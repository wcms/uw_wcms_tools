<?php

/**
 * @file
 * Functions related to the Jira API.
 *
 * @see https://developer.atlassian.com/server/jira/platform/rest-apis/
 * @see https://docs.atlassian.com/software/jira/docs/api/REST/7.12.0/
 */

define('UW_WCMS_TOOLS_JIRA_URL_PREFIX', 'https://uwaterloo.atlassian.net/browse/');
define('UW_WCMS_TOOLS_JIRA_API_LOCATION', 'https://uwaterloo.atlassian.net/rest/api/2/');

/**
 * Return the Jira Personal Access Token (PAT).
 *
 * Call variable_get() if it exists, otherwise use environment variables.
 *
 * @return string
 *   The token.
 */
function uw_wcms_tools_get_jira_credentials(): string {
  if (function_exists('variable_get')) {
    $jira_access_token = variable_get('jira_access_token');
  }
  else {
    $jira_access_token = getenv('JIRA_ACCESS_TOKEN');
  }
  if (!$jira_access_token) {
    throw new Exception('Jira API credentials not configured.');
  }
  return $jira_access_token;
}

/**
 * Run a query on the Jira API.
 *
 * @param string $path
 *   The path representing the API command.
 * @param object $data
 *   The content to send to the API. When NULL, use GET.
 * @param string $method
 *   The HTTP method to use when $data is set.
 *
 * @return object|null
 *   The decoded response from the API or NULL if an error occurs.
 */
function uw_wcms_tools_jira_api_query($path, stdClass $data = NULL, $method = 'POST') {
  if (!in_array($method, ['GET', 'POST', 'PUT'], TRUE)) {
    throw new Exception('Unsupported HTTP method.');
  }

  // Method is GET unless there is data.
  $options = [
    'http' => [
      'method' => 'GET',
      'header' => [
        'Authorization: Bearer ' . uw_wcms_tools_get_jira_credentials(),
      ],
    ],
  ];

  if ($data) {
    $options['http']['method'] = $method;
    $options['http']['header'][] = 'Content-Type: application/json';
    $options['http']['content'] = json_encode($data);
  }

  $options['http']['header'] = implode("\r\n", $options['http']['header']);
  $context = stream_context_create($options);

  $url = UW_WCMS_TOOLS_JIRA_API_LOCATION . $path;
  $results = file_get_contents($url, FALSE, $context);
  if ($results) {
    return json_decode($results);
  }
}

/**
 * Create a Jira ticket.
 *
 * @param object $jira_ticket
 *   An object containing the fields for the ticket. See the Jira API
 *   documentation.
 */
function uw_wcms_tools_jira_api_ticket_create(stdClass $jira_ticket) {
  $result = uw_wcms_tools_jira_api_query('issue', $jira_ticket);
  if (is_object($result)) {
    foreach ($result as $key => $value) {
      $jira_ticket->$key = $value;
    }
  }
}

/**
 * Create a Jira ticket for an RT.
 *
 * @param int $id
 *   The number of the RT.
 * @param array $links
 *   Links to add to the RT in addition to the link back to the Jira ticket.
 */
function uw_wcms_tools_create_jira_for_rt($id, array $links = []) {
  // Validate the RT ID.
  $id = (int) $id;
  if (!$id) {
    throw new Exception('No RT ID provided.');
  }

  // Load the RT.
  require_once 'uw_wcms_tools.rt.inc';
  $rt = uw_wcms_tools_rt_api_get_connection();

  $rt_ticket = $rt->getTicketProperties($id);
  $rt_ticket['Subject'] = trim(isset($rt_ticket['Subject']) ? $rt_ticket['Subject'] : NULL);
  if (!$rt_ticket['Subject']) {
    throw new Exception('RT has no subject.');
  }

  // Create Jira ticket.
  $jira_ticket = (object) [
    'fields' => [
      'project' => [
        'key' => 'ISTWCMS',
      ],
      'summary' => $rt_ticket['Subject'],
      // Field 'description' also available.
      'customfield_10300' => UW_WCMS_TOOLS_RT_URL_PREFIX . $id,
      'issuetype' => [
        'name' => 'Task',
      ],
    ],
  ];

  // Copy content of first history node of the RT to the description in Jira.
  $rt_ticket_first_history_node = uw_wcms_tools_rt_api_get_ticket_history_node($id);
  if (!empty($rt_ticket_first_history_node['Content'])) {
    $jira_ticket->fields['description'] = $rt_ticket_first_history_node['Content'];
  }

  uw_wcms_tools_jira_api_ticket_create($jira_ticket);

  // Add to the RT ticket a link to the new Jira ticket.
  if (isset($jira_ticket->key)) {
    $links['ReferredToBy'] = UW_WCMS_TOOLS_JIRA_URL_PREFIX . $jira_ticket->key;
  }

  foreach ($links as $relationship => $target) {
    $rt->addTicketLink($id, $relationship, $target);
  }

  return $jira_ticket;
}
