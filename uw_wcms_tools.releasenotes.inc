<?php

/**
 * @file
 * Functions related to generating release notes.
 */

/**
 * Generate release notes for the WCMS.
 *
 * @param string $start
 *   The revision to start at.
 * @param string $end
 *   The revision to end at.
 *
 * @return string
 *   The release notes.
 */
function uw_wcms_tools_release_notes_generate(string $start, string $end): string {
  $tags = [
    'start' => $start,
    'end' => $end,
  ];

  // Load composer.json from the projects to scan from the start and end
  // revision. Make an array of required project names and the version of each
  // at the start and end revisions.
  // If a project required in more than one composer.json file, the last one in
  // that stage is used.
  $projects = [];
  $projects_to_scan = [
    'wcms/drupal-recommended-project',
    'wcms/uw_base_profile',
  ];
  foreach ($projects_to_scan as $project) {
    foreach ($tags as $stage => $tag) {
      $composer = uw_wcms_tools_gitlab_get_file($project, 'composer.json', $tag);
      if ($composer['http_status'] !== 200) {
        throw new Exception('Unable to load composer.json.');
      }
      $composer = json_decode($composer['body']->content);

      foreach (($composer->require ?? []) as $name => $version) {
        $projects[$name][$stage] = $version;
      }
    }
  }

  // Generate a list of projects that have been updated.
  $release_notes = "Projects updated\n";
  foreach ($projects as $name => &$versions) {
    foreach (array_keys($tags) as $stage) {
      $versions[$stage] = $versions[$stage] ?? NULL;
    }

    if ($versions['start'] === $versions['end']) {
      continue;
    }

    $release_notes .= sprintf('%-40s: ', $name);

    if (!$versions['start']) {
      $release_notes .= 'Added';
    }
    elseif (!$versions['end']) {
      $release_notes .= 'Removed';
    }
    else {
      $release_notes .= sprintf('Upgraded from %s to %s', $versions['start'], $versions['end']);
    }

    $release_notes .= "\n";
  }

  $release_notes .= "\n";

  // Add root project so that changes to it are included in the commit and
  // ticket output.
  $projects['wcms/drupal-recommended-project'] = $tags;

  ksort($projects);

  uw_wcms_tools_gitlab_get_project_commits($projects);

  // Generate a list of updated WCMS projects and the commits each has had.
  $release_notes .= "Updates to WCMS projects\n";
  foreach ($projects as $path => $commits) {
    // Skip projects with no commits.
    if (!$commits['commits']) {
      continue;
    }
    list(, $project) = explode('/', $path, 2);

    // Make array of commit messages for this project for this release.
    $project_commits = [];
    foreach ($commits['commits'] as $commit) {
      $project_commits[] = trim($commit->title);
    }
    // Sort and remove duplicates.
    sort($project_commits);
    $project_commits = array_unique($project_commits);

    // Format list of commit messages.
    $release_notes .= $project . "\n\t";
    $release_notes .= implode("\n\t", $project_commits);
    $release_notes .= "\n";
  }

  $release_notes .= "\n";

  // Generate a list of Jira tickets acted on.
  $release_notes .= "Tickets acted on\n";
  $tickets = [];
  // Make array with keys being the ticket numbers from two-dimensional array of
  // commits.
  foreach ($projects as $path => $commits) {
    if (!$commits['commits']) {
      continue;
    }
    foreach ($commits['commits'] as $commit) {
      if (preg_match('/ISTWCMS-\d+/', $commit->title, $matches)) {
        foreach ($matches as $match) {
          $tickets[$match] = TRUE;
        }
      }
    }
  }
  $tickets = array_keys($tickets);
  // Sort tickets and output with titles from Jira.
  sort($tickets);
  $use_jira = TRUE;
  foreach ($tickets as $ticket) {
    $release_notes .= $ticket;
    // If Jira API is available, show ticket titles. On failure, do not re-try.
    if ($use_jira) {
      $jira_ticket = @uw_wcms_tools_jira_api_query('issue/' . $ticket);
      if (isset($jira_ticket->fields->summary)) {
        $release_notes .= ': ' . $jira_ticket->fields->summary;
        if ($jira_ticket->fields->status->name !== 'Done') {
          $release_notes .= ' (' . $jira_ticket->fields->status->name . ')';
        }
      }
      else {
        $use_jira = FALSE;
      }
    }
    $release_notes .= "\n";
  }

  return $release_notes;
}
