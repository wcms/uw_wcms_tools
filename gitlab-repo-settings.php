#!/usr/bin/env php
<?php

/**
 * @file
 * Set project settings for GitLab projects.
 */

require_once 'devops/uw_devops.inc';
require_once 'uw_wcms_tools.gitlab.inc';

global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'gitlab-repo-settings.php all|GITLAB-PATH
Update the settings of a GitLab repository. Provide either a GITLAB-PATH, such
as "wcms/uw_base_profile" or "all" to do all of them.';
min_args($argv, 1);

if ($argv[1] === 'all') {
  // Iterate through all WCMS-related groups.
  $groups = [
    'wcms',
    'drupal-org',
    'libraries',
    'responsive-web-design',
    'wcms-decoupled',
    'wcms-openscholar',
    'wcms-architecture',
  ];
  foreach (uw_wcms_tools_get_projects($groups) as $group) {
    foreach ($group as $project) {
      update_project($project);
    }
  }
}
// Single project.
else {
  // Match either a full GitLab URL or just the path.
  preg_match(',([^/:]+)/([^/:]+?)(?:.git)?$,', $argv[1], $matches);
  $project = uw_wcms_tools_get_project($matches[1], $matches[2]);
  update_project($project);
}

/**
 * Update a single project.
 *
 * @param object $project
 *   The project object to act on.
 */
function update_project(stdClass $project): void {
  echo $project->id . ': ' . $project->path_with_namespace . "\n";
  uw_wcms_tools_gitlab_project_ms_teams_integration_update($project->id);
  uw_wcms_tools_gitlab_project_update_features($project->id);

  // If a project has any hooks, output information about them.
  $hooks = uw_wcms_tools_gitlab_project_list_hooks($project->id);
  if ($hooks['body']) {
    var_dump($hooks);
  }
}
