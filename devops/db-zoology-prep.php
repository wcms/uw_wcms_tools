#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'db-zoology-prep.php';
min_args($argv, 0);

try {
  $tempdir = get_tempdir();
  $output = $tempdir . '/zoology.mysql';
  $files = db_dump('staging', 'zoology', 'training', $tempdir);
  // Replace "zoology" with "USERID".
  system_command('cat ' . escapeshellarg($files['filtered']) . ' | sed -e ' . escapeshellarg('s,wcms-training.uwaterloo.ca/zoology,wcms-training.uwaterloo.ca/USERID,g') . ' -e ' . escapeshellarg('s,/ca.zoology,/ca.USERID,g') . ' -e ' . escapeshellarg('s,/zoology/,/USERID/,g') . ' > ' . escapeshellarg($output));
  system_command('rsync ' . $output . ' wcms-wkr@wcms-training.uwaterloo.ca:/home/wcms-wkr/training_image');
  remove_tempdir();
}
catch (Exception $e) {
  msg($e->getMessage());
}
