## Currently (Nov. 2022) this is a work in progress
## This shouldn't be used within jenkins in it's current state.

if [ $# -lt 6 ]; then
  echo 'Not enough parameters.'
  echo 'Usage: migration_export.sh FROMSITEURL POOL DUMPDIR_SITENAME DUMPDIR SITE_FRIENDLY_NAME YOUR_EMAIL'
fi
FROMSITEURL=$1
POOL=$2
DUMPDIR_SITENAME=$3
DUMPDIR=$4
SITE_FRIENDLY_NAME=$5
YOUR_EMAIL=$6
echo "Submitted values..."
echo $FROMSITEURL
echo $POOL
echo $DUMPDIR_SITENAME
echo $DUMPDIR
echo $SITE_FRIENDLY_NAME
echo $YOUR_EMAIL
echo "Starting now, wish me luck!"
cd /home/wcmsadmin/sqldump_files/migrations
mkdir $DUMPDIR_SITENAME 
/home/wcmsadmin/uw_wcms_tools/devops/db-dump.php "$FROMSITEURL" "$POOL" "$DUMPDIR"
echo "Moving db-dump..."
cd /home/wcmsadmin/sqldump_files/migrations/$DUMPDIR_SITENAME
mv sqldump_filtered source.sql
rm sqldump_raw
echo "Getting drush uli output..."
/home/wcmsadmin/uw_wcms_tools/devops/site-drush.php "$FROMSITEURL" uli >> $DUMPDIR_SITENAME-uli.txt 
echo "Moving drush uli file..."
cd /home/wcmsadmin/
mv /home/wcmsadmin/$DUMPDIR_SITENAME-uli.txt /home/wcmsadmin/sqldump_files/migrations/$DUMPDIR_SITENAME/site_uli.txt 
echo "Done moving drush uli file..."
echo "Create all the other migration files we need using the variables supplied..."
cd /home/wcmsadmin/sqldump_files/migrations/$DUMPDIR_SITENAME
touch settings.php
echo "\$settings['uw_migrate_source'] = '$FROMSITEURL';" >>  settings.php
echo "\$settings['uw_migrate_site_path'] = 'sites/ca.$DUMPDIR_SITENAME/files';" >>  settings.php 
touch environment.txt
echo "LIVE" >> environment.txt 
touch sitename.txt
echo "$SITE_FRIENDLY_NAME" >> sitename.txt 
echo "Send a basic email..."
tar -zcvf $DUMPDIR_SITENAME.tgz $DUMPDIR_SITENAME
echo "Do not reply to this email. Attached is everything you need to migrate the site you requested." | mail -s "Migration files for '$SITE$
ls -alhF 
rm $DUMPDIR_SITENAME.tgz 
rm -rf $DUMPDIR_SITENAME
cd /home/wcmsadmin/uw_wcms_tools/devops
