#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'site-info.php URL-PATH';
min_args($argv, 1);

try {
  site_info($argv[1]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
