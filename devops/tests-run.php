#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'tests-run.php PROFILE URL-PATH';
min_args($argv, 2);

$profile = $argv[1];
if (!($profile = parse_profile_version($profile))) {
  throw new Exception('Invalid profile.');
}
$profile = $profile['profile_version_name'];

$url_path = $argv[2];
if (!valid_url_path($url_path)) {
  throw new Exception('Invalid url_path.');
}

$file_path = url_path_to_file_path($url_path);
$tempdir = '/tmp/uw_devops-tests-run';
$base_url = 'https://' . exec('hostname --fqdn') . '/' . $url_path;
$site_dir = '/var/www/' . $profile . '/sites/' . $file_path;

// Ensure the test site is on the profile to be tested.
site_change_profile($base_url, $profile);

if (!is_dir($site_dir) || !chdir($site_dir)) {
  echo "Error: Site directory does not exist.\n";
  exit(1);
}

echo "Cleanup...\n";

exec_local('rm -rf ' . $tempdir);
exec_local('sudo tweaksite ' . $file_path);

exec_local('drush cc all');
exec_local('drush updb -y');
exec_local('drush fra -y');
exec_local('drush cc all');
exec_local('drush en simpletest -y');
exec_local('drush en site_test -y');

echo "Cleared caches, updated the database, reverted features, and enabled the testing modules.\n";

echo "Cleaning Simple Test Environment before testing...\n";
exec_local('unset https_proxy && php /var/www/' . $profile . '/scripts/run-tests.sh --url ' . $base_url . ' --clean', $return_var);

if ($return_var) {
  echo 'Error: run-tests.sh --clean returned ' . $return_var . ".\n";
  exit($return_var);
}

echo "Starting to run SimpleTest...\n";
@mkdir($tempdir);
exec_local('unset https_proxy && php /var/www/' . $profile . '/scripts/run-tests.sh  --url ' . $base_url . ' --xml ' . $tempdir . ' "UW WCMS"', $return_var);

if ($return_var) {
  echo 'Error: run-tests.sh returned ' . $return_var . ".\n";
}
else {
  echo "Success: run-tests.sh complete.\n";
}
exit($return_var);

/**
 * Echo a command then execute it on the local host.
 *
 * @param string $command
 *   The command that will be executed.
 * @param string $return_var
 *   See http://ca.php.net/manual/en/function.exec.php.
 *
 * @return string
 *   The last line from the result of the command.
 */
function exec_local($command, &$return_var = NULL) {
  echo "\n" . $command . "\n";
  return passthru($command, $return_var);
}
