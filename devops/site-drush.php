#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'site-drush.php SITE-URL DRUSH-COMMAND...
Run a Drush command on a site.';
min_args($argv, 2);

// Remove name of script.
array_shift($argv);

// Get site URL.
$site_url = array_shift($argv);
$site = parse_site_url($site_url);

// Rest of arguments are the Drush command.
foreach ($argv as $key => $arg) {
  $argv[$key] = escapeshellarg($arg);
}
$command = implode(' ', $argv);

fwrite(STDERR, 'Running command: drush ' . $command . "\n");
echo drush_command($command, $site['pool'], $site['url_path']) . "\n";
