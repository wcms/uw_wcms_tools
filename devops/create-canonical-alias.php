#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'create-canonical-alias.php SITE-URL ALIAS';
min_args($argv, 2);

try {
  // Remove name of command.
  array_shift($argv);
  $site_url = array_shift($argv);
  $alias = array_shift($argv);
  create_canonical_alias($site_url, $alias);
}
catch (Exception $e) {
  msg($e->getMessage());
}
