<?php

/**
 * @file
 * Functions relating to managing files.
 */

/**
 * Copies site files from one pool and/or path to another with URL parameters.
 *
 * @param string $from
 *   The URL of the site to copy from.
 * @param string $to
 *   The pool or URL of the site to copy to.
 */
function copy_site_files_url($from, $to) {
  // $from is always a URL.
  try {
    $from = parse_site_url($from);
  }
  catch (Exception $e) {
    throw new Exception('Invalid "from" URL: ' . $e->getMessage());
  }
  $from_pool = $from['pool'];
  $from_url_path = $from['url_path'];

  // If $to is a valid pool, copy to that. Otherwise, assume it is a site URL
  // and copy to that.
  if (valid_pool($to)) {
    $to_pool = $to;
    $to_url_path = NULL;
  }
  else {
    try {
      $to = parse_site_url($to);
    }
    catch (Exception $e) {
      throw new Exception('Invalid "to" URL: ' . $e->getMessage());
    }
    $to_pool = $to['pool'];
    $to_url_path = $to['url_path'];
  }

  return copy_site_files($from_pool, $to_pool, $from_url_path, $to_url_path);
}

/**
 * Copies site files from one pool and/or path to another.
 *
 * @param string $from_pool
 *   The pool to copy from.
 * @param string $to_pool
 *   The pool to copy to.
 * @param string $from_url_path
 *   The URL path to copy from.
 * @param string $to_url_path
 *   The URL to copy to. If omitted, use $from_url_path.
 */
function copy_site_files($from_pool, $to_pool, $from_url_path, $to_url_path = NULL) {
  echo "Starting copying site files...\n";

  // Validate arguments.
  if (!valid_pool($from_pool)) {
    throw new Exception('Invalid "from" pool.');
  }
  if (!valid_pool($to_pool)) {
    throw new Exception('Invalid "to" pool.');
  }
  if (!valid_url_path($from_url_path)) {
    throw new Exception('Invalid URL path.');
  }
  if (isset($to_url_path) && !valid_url_path($to_url_path)) {
    throw new Exception('Invalid "to" URL path.');
  }

  $file_from_path = url_path_to_file_path($from_url_path);
  // If $to_url_path is not set, it is the same as $from_url_path.
  if ($to_url_path) {
    $file_to_path = url_path_to_file_path($to_url_path);
  }
  else {
    $file_to_path = $file_from_path;
    $to_url_path = $from_url_path;
  }

  // Ensure to and from are not the same place.
  if ($from_pool === $to_pool && $file_from_path === $file_to_path) {
    throw new Exception('"from" and "to" are the same place.');
  }

  $file_root = '/allfiles';
  $from_root = $file_root . '/' . $from_pool;
  $to_root = $file_root . '/' . $to_pool;

  // Check for existance of from and to public directories.
  $test_dir = $from_root . '/public/' . $file_from_path;
  if (!is_dir($test_dir)) {
    throw new Exception('No "from" site directory: ' . $test_dir);
  }
  $test_dir = $to_root . '/public/' . $file_to_path;
  if (!is_dir($test_dir)) {
    throw new Exception('No "to" site directory: ' . $test_dir);
  }

  // Copy files from the site directories for one site to another.
  foreach (glob($from_root . '/{private,public,temp}/' . $file_from_path, GLOB_ONLYDIR | GLOB_BRACE) as $dir) {
    // Remove "$from_root/" from beginning of $dir and "/$file_from_path"
    // from the end.
    $dir = substr($dir, strlen($from_root . '/'));
    $dir = substr($dir, 0, -strlen('/' . $file_from_path));

    $from_dir = $from_root . '/' . $dir . '/' . $file_from_path . '/.';
    $to_dir = $to_root . '/' . $dir . '/' . $file_to_path . '/.';

    system_command('sudo rsync --archive --delete --progress ' . escapeshellarg($from_dir) . ' ' . escapeshellarg($to_dir));
  }

  tweaksite($to_pool, $to_url_path);
}
