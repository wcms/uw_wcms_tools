#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'create-site.php SITE-URL PROFILE-VERSION [SITE-INSTALL-ARGUMENTS...]
PROFILE-VERSION must be like "<profile_name>-<drupal_version>-<profile_version>".
Any arguments after PROFILE-VERSION will be sent through to drush site-install.';
min_args($argv, 2);

// Remove script name.
array_shift($argv);
$site_url = array_shift($argv);
$profile_version_name = array_shift($argv);

try {
  create_site($site_url, $profile_version_name, $argv);
}
catch (Exception $e) {
  msg($e->getMessage());
}
