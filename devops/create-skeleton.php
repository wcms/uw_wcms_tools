#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'create-skeleton.php SITE-URL PROFILE-VERSION [REDIRECT...]
PROFILE-VERSION must be like "<profile_name>-<drupal_version>-<profile_version>".';
min_args($argv, 2);

try {
  // Remove name of command.
  array_shift($argv);
  $site_url = array_shift($argv);
  $profile_version_name = array_shift($argv);
  $redirects = $argv;
  create_skeleton($site_url, $profile_version_name, $redirects);
}
catch (Exception $e) {
  msg($e->getMessage());
}
