#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'db-copy.php FROM-SITE-URL TO-POOL';
min_args($argv, 2);

try {
  db_copy($argv[1], $argv[2]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
