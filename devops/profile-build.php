#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'profile-build.php POOL PROFILE

If the profile already exists, it will rebuild it leaving Drupal alone.';
min_args($argv, 2);

try {
  profile_build($argv[1], $argv[2]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
