#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'delete-site.php [--no-backup] SITE-URL
Does not work for the root site.';
min_args($argv, 1);

$options = getopt('', ['no-backup']);
$make_backup = !isset($options['no-backup']);

$site_url = array_pop($argv);

try {
  delete_site($site_url, $make_backup);
}
catch (Exception $e) {
  msg($e->getMessage());
}
