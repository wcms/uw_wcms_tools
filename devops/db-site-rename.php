#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'db-site-rename.php DUMP-FILENAME OLD-SITE-PATH NEW-SITE-PATH

Processes a database dump file to rename the site path. The updated version is written to standard output.';
min_args($argv, 3);

$dump_filename = $argv[1];
$old = $argv[2];
$old_file_path = str_replace('/', '.', $old);
$real_new = $argv[3];
$real_new_file_path = str_replace('/', '.', $real_new);

$length_change = strlen($real_new) - strlen($old);

$new = 'QAZXSWEDCVFRTGBNHYUJMKIOLP';
$new_file_path = $new . '-FILE-PATH';
$replacements = [
  ",/$old/node/(\d+)," => "/$new/node/\\1",
  ",/$old/sites/ca\.$old_file_path/," => "/$new/sites/ca.$new_file_path/",
  // Doesn't handle not ending in "/". Next is: \\".
  // @code
  // ",https?://([a-z0-9-]+\.)?uwaterloo\.ca/$old/," => "/$new/",
  // @endcode
  ",([\"'])/$old/(?!admin/reports/fields)," => "\\1/$new/",
  ",/ca\.$old_file_path/," => "/ca.$new_file_path/",
  ',' . $new_file_path . ',' => $real_new_file_path,
  ',' . $new . ',' => $real_new,
  // For GSAC archive links.
  ',/graduate-studies-academic-calendar/archive-[^/]+/archive-,' => '/graduate-studies-academic-calendar/archive-',
];

$line_length_change_total = 0;

$file = fopen($dump_filename, 'r');
while ($line = fgets($file)) {
  // Replaces many serialized strings.
  //
  // This does not handle all possibilities. One problem is variables that
  // contain HTML links that need to get renamed. The variable serialization
  // length updater doesn't go past a double-quote. Double quotes will appear
  // as '\"', but the backslash is not counted in the serialize length.
  $new_line = preg_replace_callback(
    ',s:(\d+):\\\"([^"]+)' . $old . '/,',
    function ($matches) use ($new, $length_change) {
      return 's:' . ($matches[1] + $length_change) . ':\"' . $matches[2] . $new . '/';
    },
    $line
  );

  // Do the actual replacements.
  $new_line = preg_replace(array_keys($replacements), $replacements, $new_line);

  // Raise warning for line length change in field_config and variable.
  $line_length_change = strlen($new_line) - strlen($line);
  $line_length_change_total += $line_length_change;
  foreach (['field_config', 'variable', 'system'] as $table) {
    if (strpos($line, 'INSERT INTO `' . $table . '` VALUES') === 0 && $line_length_change !== 0) {
      fwrite(STDERR, "Warning: $table table may need serialized field lengths to be updated manually.\n");
      fwrite(STDERR, "\tLine length change: " . $line_length_change . ", which suggests " . abs($line_length_change / $length_change) . " replacements.\n");
    }
  }

  echo $new_line;
}
fwrite(STDERR, "line_length_change_total: $line_length_change_total\n");
fwrite(STDERR, "Done\n");
