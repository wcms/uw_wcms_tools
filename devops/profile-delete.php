#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'profile-delete.php POOL PROFILE';
min_args($argv, 2);

try {
  profile_delete($argv[1], $argv[2]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
