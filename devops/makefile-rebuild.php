#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'makefile-rebuild.php SITE-URL';
min_args($argv, 1);

try {
  site_makefile_rebuild($argv[1]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
