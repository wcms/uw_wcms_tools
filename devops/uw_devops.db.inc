<?php

/**
 * @file
 * Functions relating to databases.
 */

/**
 * Return the name of the database used by a site.
 *
 * An exception is thrown if either parameter is invalid.
 *
 * This is: first-2-letters-of-<pool> drupal-main-version "_"
 * <ca.site-name>-with-hyphens-and-periods-removed
 * all truncated to 60 characters and lowercase.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 * @param int $drupal_major_version_number
 *   The major Drupal version.
 *
 * @return string
 *   The name of the database.
 */
function url_path_to_db_name($pool, $url_path, $drupal_major_version_number) {
  if (!valid_pool($pool)) {
    throw new Exception('Invalid pool.');
  }
  $file_path = url_path_to_file_path($url_path);

  $database_prefix = substr($pool, 0, 2) . $drupal_major_version_number;
  $file_path = str_replace(['-', '.'], '', $file_path);
  return strtolower(substr($database_prefix . '_' . $file_path, 0, 60));
}

/**
 * Test the existance of a database.
 *
 * @param string $db_host
 *   The name of the database host.
 * @param string $db_name
 *   The name of the database.
 *
 * @return bool
 *   TRUE if the database exists, FALSE otherwise.
 */
function database_exists($db_host, $db_name) {
  exec('mysql --batch --skip-column-names --host=' . escapeshellarg($db_host) . ' --execute=' . escapeshellarg("SHOW DATABASES LIKE '$db_name'"), $output);
  return (bool) $output;
}

/**
 * Copy a site's database from one pool to another.
 *
 * @param string $from
 *   The URL of the site to copy from.
 * @param string $to_pool
 *   The pool to copy to.
 */
function db_copy($from, $to_pool) {
  echo "Starting copying database...\n";
  $from = parse_site_url($from);
  if (!valid_pool($to_pool)) {
    throw new Exception('Invalid "to" pool.');
  }

  if ($from['pool'] === $to_pool) {
    throw new Exception('"from" and "to" are the same place.');
  }

  // Test for site existance.
  get_drupal_root($from['pool'], $from['url_path']);
  get_drupal_root($to_pool, $from['url_path']);

  echo "Clearing linkchecker cache...\n";
  drush_command('linkchecker-clear', $from['pool'], $from['url_path']);

  $tempdir = get_tempdir();
  $dump_files = db_dump($from['pool'], $from['url_path'], $to_pool, $tempdir);

  db_import($to_pool, $from['url_path'], $dump_files['filtered']);

  remove_tempdir();
}

/**
 * Export a database and make minor modifications, preparing it for import.
 *
 * @param string $from_pool
 *   The pool of the site to dump.
 * @param string $url_path
 *   The url_path of the site to dump.
 * @param string $to_pool
 *   The pool to use in the modifications to the dump.
 * @param string $destination_dir
 *   The directory in which to put the dump files.
 *
 * @return array
 *   Array of filenames of the DB dump files keyed with "raw" and "filtered" for
 *   the raw dump file and the one that has been modified, respectively.
 */
function db_dump($from_pool, $url_path, $to_pool, $destination_dir) {
  echo "Dumping database...\n";
  if (!valid_pool($from_pool)) {
    throw new Exception('Invalid "from" pool.');
  }
  if (!valid_url_path($url_path)) {
    throw new Exception('Invalid url_path.');
  }
  if (!valid_pool($to_pool)) {
    throw new Exception('Invalid "to" pool.');
  }

  $sqldump_raw = $destination_dir . '/sqldump_raw';
  $sqldump_filtered = $destination_dir . '/sqldump_filtered';

  $pool_info = pool_info();
  $settings = parse_settings_file($from_pool, $url_path);

  // Dump database to a file.
  system_command('mysqldump --single-transaction --set-gtid-purged=OFF --host=' . escapeshellarg($settings['dbhost']) . ' ' . escapeshellarg($settings['UWdb']) . ' > ' . escapeshellarg($sqldump_raw));

  // Make minor modifications to database. Remove cache, watchdog,
  // fast_watchdog, and linkchecker_link tables and lock commands. Update any
  // self-referrential fully-qualified URLs to point to the new host.
  system_command('cat ' . escapeshellarg($sqldump_raw) . ' | egrep -v \'^INSERT INTO `cache\' | egrep -v \'^INSERT INTO `watchdog\' | egrep -v \'^INSERT INTO `fast_watchdog\' | egrep -v \'^INSERT INTO `linkchecker_link\' | egrep -v \'LOCK TABLE\' | sed -e ' . escapeshellarg('s,https?://[^.]*\.?uwaterloo.ca/' . $url_path . ',https://' . $pool_info[$to_pool]['host'] . 'uwaterloo.ca/' . $url_path . ',g') . ' > ' . escapeshellarg($sqldump_filtered));

  return array('raw' => $sqldump_raw, 'filtered' => $sqldump_filtered);
}

/**
 * Load a database dump file into a database.
 *
 * @param string $pool
 *   The pool of the site into which to load the dump.
 * @param string $url_path
 *   The url_path of the site into which to load the dump.
 * @param string $dump_file
 *   The filename of the dump file to load.
 * @param bool $drop
 *   Drop and re-create the database before import.
 */
function db_import($pool, $url_path, $dump_file, $drop = TRUE) {
  echo "Importing database...\n";
  if (!valid_pool($pool)) {
    throw new Exception("Invalid pool $pool.");
  }
  if (!valid_url_path($url_path)) {
    throw new Exception("Invalid url_path $url_path.");
  }
  if (!is_readable($dump_file)) {
    throw new Exception("Invalid filename or unable to read $dump_file.");
  }

  $settings = parse_settings_file($pool, $url_path);
  $db_name = $settings['UWdb'];

  // Drop and re-create target database.
  if ($drop) {
    system_command('mysql --host=' . escapeshellarg($settings['dbhost']) . ' --execute=' . escapeshellarg('DROP DATABASE IF EXISTS ' . $db_name . '; CREATE DATABASE ' . $db_name));
  }

  // Load modified dump file into the new database.
  system_command('mysql --host=' . escapeshellarg($settings['dbhost']) . ' ' . escapeshellarg($db_name) . ' < ' . escapeshellarg($dump_file));

  // Delete CAS users from user 1.
  system_command('mysql --host=' . escapeshellarg($settings['dbhost']) . ' ' . escapeshellarg($db_name) . ' --execute=' . escapeshellarg('DELETE FROM cas_user WHERE uid = 1'));
}
