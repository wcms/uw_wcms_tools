#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'site-adjust.php SITE-URL
Adjust modules, clear-cache, and check that a site is up.';
min_args($argv, 1);

try {
  $site = parse_site_url($argv[1]);
  $pool = $site['pool'];
  $url_path = $site['url_path'];

  adjust_site($pool, $url_path);
  drush_site_cache_clear($pool, $url_path);
  site_is_up($pool, $url_path);
}
catch (Exception $e) {
  msg($e->getMessage());
}
