<?php

/**
 * @file
 * Functions relating to managing sites.
 */

/**
 * Create the files directories and database for a site on a pool.
 *
 * @param string $site_url
 *   The URL of the site to create.
 * @param string $profile_version_name
 *   The name and version of the profile to install.
 * @param string[] $redirects
 *   An array of URL paths which will redirect to the site.
 * @param bool $utf8mb4
 *   TRUE if the skeleton should be set to use Database 4 byte UTF-8 support,
 *   FALSE otherwise.
 */
function create_skeleton($site_url, $profile_version_name, array $redirects = array(), $utf8mb4 = FALSE) {
  echo "Starting creating skeleton...\n";
  $site_url = parse_site_url($site_url);
  $pool = $site_url['pool'];
  $url_path = $site_url['url_path'];
  exclude_root_site($url_path);

  $profile = parse_profile_version($profile_version_name);
  $profile_version_name = $profile['profile_version_name'];

  echo 'Creating "' . $url_path . '" on ' . $pool . "...\n";

  $pool_info = pool_info();
  $engine = $pool_info[$pool]['engine'];
  $dbmach = $pool_info[$pool]['dbmach'];

  echo "Checking if destination profile exists...\n";
  if (!$drupal_root = valid_drupal_root($engine[0], WEB_ROOT . '/' . $profile_version_name)) {
    throw new Exception('Profile does not exist on host.');
  }
  $drupal_root = $drupal_root['drupal_root'];

  foreach ($redirects as $redirect) {
    if (!valid_url_path($redirect)) {
      throw new Exception('Invalid redirect.');
    }
  }

  $file_path = url_path_to_file_path($url_path);
  $file_path_root = $drupal_root . '/sites/' . $file_path;

  // Stop if site directory already exists.
  echo "Checking if site already exists...\n";
  if (get_profile_version($pool, $url_path)) {
    throw new Exception('Site already exists.');
  }

  // Generate: dbuser, dbpasswd, dbname.
  // Random generation of dbuser, dbpassword.
  echo "Generating passwords...\n";
  $dbuser = generate_password(TRUE);
  $dbpasswd = generate_password();
  if (!($dbuser && $dbpasswd)) {
    throw new Exception('Unable to generate passwords.');
  }
  $dbname = url_path_to_db_name($pool, $url_path, $profile['drupal_major_version_number']);

  // Directories to create once.
  // Due to shared files, this only needs to be run on one host in the pool.
  echo "Installing on wmsfiles...\n";
  $file_types = array('public', 'private', 'temp');
  $create_dirs_once = array();
  foreach ($file_types as $type) {
    $create_dirs_once[] = '/wmsfiles/' . $type . '/' . $file_path;
  }
  // Manually creating uploads directory to inherit proper permissions.
  $create_dirs_once[] = '/wmsfiles/public/' . $file_path . '/uploads';

  remote_create_dir($engine[0], $create_dirs_once);
  // Install the default .htaccess files to keep Drupal happy.
  foreach ($file_types as $dir) {
    ssh_exec($engine[0], 'cp /wmsfiles/' . $dir . '/.htaccess /wmsfiles/' . $dir . '/' . $file_path . '/.htaccess');
  }

  // Directories to create on each server.
  $create_dirs_all = array(
    $file_path_root . '/modules',
    $file_path_root . '/themes',
    $file_path_root . '/libraries',
  );

  // Generate sites/ca.site_url/settings.php (short version)
  // Output as boolean.
  $utf8mb4 = $utf8mb4 ? 'TRUE' : 'FALSE';
  $settings_file = "<?php
// VERSION = 1.3;

\$UWalt = 0;
\$UWdb = '$dbname';
\$UWuser = '$dbuser';
\$UWpass = '$dbpasswd';
\$UWpref = '$url_path';
\$utf8mb4 = $utf8mb4;

require_once DRUPAL_ROOT . '/profiles/uw_base_profile/drupal-settings.php';";

  // Create site directories and copy config files.
  foreach ($engine as $eng) {
    echo "Installing on $eng...\n";
    remote_create_dir($eng, $create_dirs_all);

    $command = 'ln -s ' . escapeshellarg('/wmsfiles/public/' . $file_path) . ' ' . escapeshellarg($file_path_root . '/files');
    ssh_exec($eng, $command);

    $command = 'echo ' . escapeshellarg($settings_file) . ' > ' . $file_path_root . '/settings.php';
    ssh_exec($eng, $command);
  }
  tweaksite($pool, $url_path);

  create_site_apache_config($pool, $url_path, $profile_version_name, $redirects);

  try {
    apache_restart($pool);
  }
  catch (Exception $e) {
    echo 'WARNING: ' . $e->getMessage() . "\n";
  }

  echo "Building site makefile...\n";
  try {
    site_makefile_rebuild(build_site_url($pool, $url_path));
  }
  catch (Exception $e) {
    echo $e->getMessage() . "\n";
  }

  // Create database and user, and set permissions.
  $settings = parse_settings_file($pool, $url_path);
  $sql = "CREATE DATABASE IF NOT EXISTS $dbname;
CREATE USER '$dbuser'@'$dbmach.uwaterloo.ca' IDENTIFIED BY '$dbpasswd';
GRANT ALTER, CREATE, CREATE TEMPORARY TABLES, DELETE, DROP, INDEX, INSERT, LOCK TABLES, SELECT, UPDATE
  ON $dbname.*
  TO '$dbuser'@'$dbmach.uwaterloo.ca';
FLUSH PRIVILEGES;";
  system_command('mysql --host=' . escapeshellarg($settings['dbhost']) . ' --execute=' . escapeshellarg($sql));
}

/**
 * Add redirects to a site.
 *
 * @param string $site_url
 *   The URL of the site.
 * @param string[] $redirects
 *   An array of URL paths which will redirect to the site.
 *
 * @see delete_redirects()
 */
function create_redirects($site_url, array $redirects) {
  echo "Starting creating redirects...\n";

  $site_url = parse_site_url($site_url);
  $pool = $site_url['pool'];
  $url_path = $site_url['url_path'];
  exclude_root_site($url_path);

  $config_apache = get_site_config_apache($pool, $url_path);
  $redirects = array_merge($redirects, $config_apache['redirects']);
  $aliases = $config_apache['aliases'];
  create_site_apache_config($pool, $url_path, NULL, $redirects, $aliases, $config_apache['decoupled']);

  apache_restart($pool);
}

/**
 * Delete redirects from a site.
 *
 * @param string $site_url
 *   The URL of the site.
 * @param string[] $redirects
 *   An array of URL paths which will redirect to the site.
 *
 * @see create_redirects()
 */
function delete_redirects($site_url, array $redirects) {
  echo "Starting deleting redirects...\n";

  $site_url = parse_site_url($site_url);
  $pool = $site_url['pool'];
  $url_path = $site_url['url_path'];
  exclude_root_site($url_path);

  $config_apache = get_site_config_apache($pool, $url_path);
  $redirects = array_diff($config_apache['redirects'], $redirects);

  $aliases = $config_apache['aliases'];
  create_site_apache_config($pool, $url_path, NULL, $redirects, $aliases, $config_apache['decoupled']);

  apache_restart($pool);
}

/**
 * Define the canonical public URL for a site.
 *
 * This function creates the configuration which gives a site a public URL which
 * is different from the URL path used for the site directory name. This is
 * normally used to create sites with non-alphnumeric URLs.
 *
 * Outline of the steps that this function will do:
 *   ln -s .../sites/ca.OLD .../sites/ca.OTHER
 *   In ca.OLD/settings.php:
 *     Change $UWpref from 'OLD' to 'OTHER'.
 *     Add $UW_file_path = 'OLD'.
 *   In /wmsfiles/apache/builds/* /aliases/ca.OLD.Pconf: duplicate alias
 *   directive with OLD replaced with OTHER.
 *   In /wmsfiles/apache/builds/* /rewrites/ca.OLD.Pconf: duplicate contents
 *   with OLD replaced with OTHER.
 *
 * @param string $site_url
 *   The URL of the site.
 * @param string $alias
 *   The URL path to use as the canonical public site URL path.
 */
function create_canonical_alias($site_url, $alias) {
  echo "Starting creating alias...\n";

  $site_url = parse_site_url($site_url);
  $pool = $site_url['pool'];
  $url_path = $site_url['url_path'];
  $file_path = $site_url['file_path'];
  exclude_root_site($url_path);

  if (!valid_url_alias($alias)) {
    throw new Exception('Invalid alias.');
  }
  $alias_file_path = url_path_to_file_path($alias);

  $profile_version_name = get_profile_version($pool, $url_path);
  $pool_info = pool_info();
  foreach ($pool_info[$pool]['engine'] as $engine) {
    $commands = array();
    $commands[] = 'cd ' . WEB_ROOT . '/' . $profile_version_name . '/sites';
    // Create a symlink to the site directory named for the alias.
    $commands[] = 'ln --symbolic --force --no-dereference ' . $file_path . ' ' . $alias_file_path;
    $commands[] = 'cd ' . $file_path;
    // First expression removes any existing $UW_file_path.
    // Second expression replaces $UWpref with the alias and adds $UW_file_path.
    $commands[] = 'sed --in-place --regexp-extended --expression="/\\\\\\$UW_file_path/d" --expression="s/\\\\\\$UWpref.+/\$UWpref = \'' . $alias . '\';\n\$UW_file_path = \'' . $url_path . '\';/" settings.php';
    ssh_exec($engine, implode(' && ', $commands));

    // Remove any other symlinks to the site directory.
    $commands = array();
    $commands[] = 'cd ' . WEB_ROOT . '/' . $profile_version_name . '/sites';
    $commands[] = 'find -L . -maxdepth 1 -samefile ' . $file_path;
    ssh_exec($engine, implode(' && ', $commands), $same_files);
    $delete = array();
    foreach ($same_files as $file) {
      if (!in_array($file, array('./' . $file_path, './' . $alias_file_path), TRUE)) {
        $delete[] = $file;
        echo "WARNING: Deleted alias $file.\n";
      }
    }
    if ($delete) {
      $commands = array();
      $commands[] = 'cd ' . WEB_ROOT . '/' . $profile_version_name . '/sites';
      $commands[] = 'rm ' . implode(' ', array_map('escapeshellarg', $delete));
      ssh_exec($engine, implode(' && ', $commands));
    }
  }

  // Update Apache config.
  $config_apache = get_site_config_apache($pool, $url_path);
  // Throw away existing aliases because only 1 additional alias should exist.
  $aliases = array($alias);
  create_site_apache_config($pool, $url_path, NULL, $config_apache['redirects'], $aliases, $config_apache['decoupled']);

  apache_restart($pool);
}

/**
 * Return a list of URL redirects for a site.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 *
 * @return array
 *   An array with keys 'redirects' and 'aliases', each being an array of
 *   strings.
 */
function get_site_config_apache($pool, $url_path) {
  $profile_version_name = get_profile_version($pool, $url_path);
  $level = site_level($url_path);
  $file_path = url_path_to_file_path($url_path);

  $pool_info = pool_info();
  $engine = $pool_info[$pool]['engine'][0];

  // Read Apache aliases config file.
  // "2>/dev/null" suppresses "no such file" messages.
  $command = 'cat ' . escapeshellarg(APACHE_DIR . '/builds/' . $profile_version_name . '/aliases' . $level . '/' . $file_path . '.Pconf') . ' 2>/dev/null';
  ssh_exec($engine, $command, $output);

  // Make array of configuration directives.
  $config = array(
    'aliases' => array(),
    'redirects' => array(),
    'decoupled' => get_site_is_decoupled($pool, $url_path),
  );
  $name_map = array('Alias' => 'aliases', 'RedirectPermanent' => 'redirects');
  foreach ($output as $line) {
    if (preg_match(",(Alias|RedirectPermanent)\s+/(\S+)\s+\S,", $line, $backref)) {
      if ($backref[2] !== $url_path) {
        $config[$name_map[$backref[1]]][] = $backref[2];
      }
    }
  }
  return $config;
}

/**
 * Install aliases.Pconf and redirect.Pconf files for a site.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 * @param string|null $profile_version_name
 *   The name and version of the profile to install. If NULL, it will use the
 *   existing version.
 * @param string[] $redirects
 *   An array of URL paths which will redirect to the site.
 * @param string[] $aliases
 *   An array of URL paths which are aliases of the site.
 * @param bool $is_decoupled
 *   Whether the site is decoupled.
 */
function create_site_apache_config($pool, $url_path, $profile_version_name = NULL, array $redirects = array(), array $aliases = array(), $is_decoupled = FALSE) {
  echo "Starting creating Apache config...\n";

  // No config required for root site.
  $file_path = url_path_to_file_path($url_path);
  if ($url_path === '') {
    return;
  }

  // When $profile_version_name is not specified, use existing.
  if (!$profile_version_name) {
    $profile_version_name = get_profile_version($pool, $url_path);
  }

  $file_contents = array();
  $file_contents['aliases'] = '';
  $file_contents['rewrites'] = '';

  // Add Alias directive for $url_path and each additional in $aliases.
  array_unshift($aliases, $url_path);
  foreach (array_unique($aliases) as $alias) {
    if (valid_url_alias($alias)) {
      $last_alias = $alias;
      $file_contents['aliases'] .= 'Alias /' . $alias . ' ' . WEB_ROOT . '/' . $profile_version_name . "\n";

      // Add pre-renderer rewrites for decoupled sites.
      if ($is_decoupled) {
        $file_contents['rewrites'] .= "
RewriteCond %{REQUEST_URI} ^/$alias/.*
RewriteCond %{HTTP_USER_AGENT} baiduspider|facebookexternalhit|twitterbot|rogerbot|linkedinbot|embedly|quora\ link\ preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator|gsa-crawler [NC,OR]
RewriteCond %{QUERY_STRING} _escaped_fragment_
# Only proxy the request to Prerender if it's a request for HTML
RewriteRule ^(?!.*?(\.js|\.css|\.xml|\.less|\.png|\.jpg|\.jpeg|\.gif|\.pdf|\.doc|\.txt|\.ico|\.rss|\.zip|\.mp3|\.rar|\.exe|\.wmv|\.doc|\.avi|\.ppt|\.mpg|\.mpeg|\.tif|\.wav|\.mov|\.psd|\.ai|\.xls|\.mp4|\.m4a|\.swf|\.dat|\.dmg|\.iso|\.flv|\.m4v|\.torrent|\.ttf|\.woff))(.*) http://wms-nodejs.private.uwaterloo.ca:3000/https://uwaterloo.ca/$alias/\$2 [P,L]
";
      }

      // Rewrites for every site.
      $file_contents['rewrites'] .= "
RewriteCond %{REQUEST_URI} ^/$alias/.*
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)\$ /$alias/index.php?q=\$1 \"[B= ,L,QSA]\"
";
    }
    else {
      echo "WARNING: Invalid alias URL path, no alias created.\n";
    }
  }

  foreach (array_unique($redirects) as $redirect) {
    if (valid_url_alias($redirect)) {
      // The last alias is likely the one used in public so redirects
      // should be to that one.
      $file_contents['aliases'] .= "RedirectPermanent /$redirect /$last_alias\n";
    }
    else {
      echo "WARNING: Invalid redirect URL path, no redirect created.\n";
    }
  }

  $level = site_level($url_path);
  $pool_info = pool_info();

  // Only run on one host because Apache config is on shared drive.
  $engine = $pool_info[$pool]['engine'][0];

  // Remove old config.
  foreach (array('aliases', 'rewrites') as $file_type) {
    $command = 'rm --force ' . APACHE_DIR . '/builds/*/' . $file_type . '*/' . $file_path . '.Pconf';
    ssh_exec($engine, $command);
  }

  // Create new config.
  foreach (array_keys($file_contents) as $file_type) {
    $filename = APACHE_DIR . '/builds/' . $profile_version_name . '/' . $file_type . $level . '/' . $file_path . '.Pconf';
    $command = 'echo ' . escapeshellarg(trim($file_contents[$file_type])) . ' > ' . $filename;
    ssh_exec($engine, $command);
    if (!remote_file_exists($engine, $filename)) {
      throw new Exception('Unable to create file: ' . $filename);
    }
  }
}

/**
 * Create a complete site.
 *
 * @param string $site_url
 *   The URL of the site to create.
 * @param string $profile_version_name
 *   The name and version of the profile to install.
 * @param array $site_arguments
 *   An array of additional arguments to be sent to drush site-install.
 */
function create_site($site_url, $profile_version_name, array $site_arguments = NULL) {
  echo "Starting creating site...\n";
  create_skeleton($site_url, $profile_version_name);

  // $site_url is already validated in create_skeleton().
  $site_url = parse_site_url($site_url);
  $pool = $site_url['pool'];
  $url_path = $site_url['url_path'];

  drush_install_site($pool, $url_path, NULL, $site_arguments);
}

/**
 * Prepare a set of training sites for a course.
 *
 * @param string $course
 *   The name of the course for which to make course-specific adjustments.
 * @param array $usernames
 *   The WatIAM usernames of users for whom to create sites.
 */
function create_training_course_sites($course, array $usernames) {
  $courses = ['site', 'form', 'edit', 'main'];
  if (!in_array($course, $courses, TRUE)) {
    throw new Exception('Invalid course; must be one of: ' . implode(', ', $courses));
  }
  foreach ($usernames as $username) {
    if (!valid_username($username)) {
      throw new Exception('Invalid username: ' . $username);
    }
  }
  foreach ($usernames as $username) {
    create_training_site($username, $course);
  }
}

/**
 * Create a training site from the template.
 *
 * @param string $username
 *   The WatIAM username of user for whom to create the site.
 * @param string $course
 *   The name of the course for which to make course-specific adjustments.
 */
function create_training_site($username, $course = NULL) {
  if (!valid_username($username)) {
    throw new Exception('Invalid username.');
  }
  $site_url = build_site_url('training', $username);

  echo "\n\nStarting processing $site_url\n";

  $site_url_arr = parse_site_url($site_url);
  $pool = $site_url_arr['pool'];
  $url_path = $site_url_arr['url_path'];
  $file_path = $site_url_arr['file_path'];

  $pool_info = pool_info();
  $host = $pool_info[$pool]['engine'][0];

  // Only create if the site does not already exist.
  if (get_profile_version($pool, $url_path)) {
    echo "Site already exists.\n";
  }
  else {
    // Get name of latest training site template.
    $template_path = '/home/wcms-wkr/training_image';
    $profile_version_name = ssh_exec($host, 'ls --directory /var/www/uw_base_profile-* | tail --lines=1');
    $profile_version_name = preg_replace(',/var/www/,', '', trim($profile_version_name));

    echo "Creating site using template $template_path\n";

    create_skeleton($site_url, $profile_version_name);

    echo "Importing template database dump...\n";
    // Copy the database dump files into a tempdir.
    $tempdir = get_tempdir();
    exec('rsync ' . WORKER . "@$host:$template_path/*.mysql $tempdir");
    // Replace USERID with the username for the site being created.
    exec("sed --in-place --expression='s/USERID/$username/g' $tempdir/*.mysql");
    // Import the database dump files.
    db_import($pool, $url_path, $tempdir . '/zoology.mysql');
    db_import($pool, $url_path, $tempdir . '/custom.mysql', FALSE);

    echo "Extracting public files into Drupal files directory...\n";
    ssh_exec($host, "tar -m --directory=/wmsfiles/public/$file_path --extract --file=$template_path/ca.zoology.tar");

    drush_site_update_clear_cache($pool, $url_path);
  }

  echo "Making course-specific adjustments...\n";
  $course_roles = [
    'site' => 'site manager',
    'form' => 'form editor',
    'edit' => 'content editor',
  ];
  if (isset($course_roles[$course])) {
    drush_command('user-add-role --yes "' . $course_roles[$course] . '" ' . $username, $pool, $url_path);
  }
  drush_command('user-add-role --yes "form results access" ' . $username, $pool, $url_path);

  remove_tempdir();
  site_is_up($pool, $url_path);
}

/**
 * Drops the database and reinstalls a site.
 *
 * @param string $site_url
 *   The URL of the site to reinstall.
 */
function redo_site($site_url) {
  echo "Starting site redo...\n";

  $site_url = parse_site_url($site_url);
  $pool = $site_url['pool'];
  $url_path = $site_url['url_path'];

  if (!get_profile_version($pool, $url_path)) {
    throw new Exception('Site does not exist.');
  }

  drush_install_site($pool, $url_path);
}

/**
 * Run "drush site-install" and related commands to create a site.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 * @param string $profile
 *   The name of the profile to install.
 * @param array $site_arguments
 *   An array of additional arguments to be sent to drush site-install.
 */
function drush_install_site($pool, $url_path, $profile = NULL, array $site_arguments = NULL) {
  echo "Starting Drush site install...\n";

  $file_path = url_path_to_file_path($url_path);

  if (!$profile) {
    $profile = get_profile_version($pool, $url_path);
    $profile = parse_profile_version($profile);
    $profile = $profile['profile_name'];
  }

  // Convert $site_arguments into a single string.
  if ($site_arguments) {
    $site_arguments = array_map('escapeshellarg', $site_arguments);
    $site_arguments = ' ' . implode(' ', $site_arguments);
  }
  else {
    $site_arguments = NULL;
  }

  $commands = array();
  $commands[] = 'site-install --yes ' . escapeshellarg($profile) . ' --sites-subdir=' . escapeshellarg($file_path) . $site_arguments;
  $commands[] = 'updatedb --yes';
  $commands[] = 'core-cron';

  $hostname = gethostname();
  $hostname = explode('.', $hostname, 2)[0];
  if ($hostname === 'wms-aux1' || ($hostname === 'wms-sandboxes' && $pool === 'sandboxes')) {
    foreach ($commands as $command) {
      echo "Running: drush $command\n";
      drush_command($command, $pool, $url_path);
    }

    // Fix permissions on stuff created by installing modules.
    tweaksite($pool, $url_path);
  }
  else {
    throw new Exception('Site cannot be installed from this host; use wms-aux1.');
  }
  adjust_site($pool, $url_path);
  drush_site_features_revert_all($pool, $url_path);

  // Run uw_base_profile_post_install().
  echo "Running: uw_base_profile_post_install()\n";
  $php = "if (function_exists('uw_base_profile_post_install')) {uw_base_profile_post_install();}";
  drush_command('php-eval ' . escapeshellarg($php), $pool, $url_path);

  site_is_up($pool, $url_path);
}

/**
 * Adjust file permissions by running tweaksite bash script on the remote host.
 *
 * This is equivalent to:
 * @code
 *   chmod -R g+ws ' . DRUPAL_ROOT . '/sites/' . $file_path
 *   chown -R apache.webbers ' . DRUPAL_ROOT . '/sites/' . $file_path
 *   chmod a=r ' . DRUPAL_ROOT . '/sites/' . $file_path . '/settings.php'
 *   chmod -R ug+rwxs /wmsfiles/public/' . $file_path . ' /wmsfiles/private/' . $file_path
 *   chown -R apache.apache /wmsfiles/public/' . $file_path . ' /wmsfiles/private/' . $file_path
 * @endcode
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 */
function tweaksite($pool, $url_path) {
  $file_path = url_path_to_file_path($url_path);

  $pool_info = pool_info();
  $engine = $pool_info[$pool]['engine'];

  foreach ($engine as $host) {
    echo "Adjusting permissions on $host...\n";
    ssh_exec($host, 'sudo tweaksite ' . escapeshellarg($file_path));
  }
}

/**
 * Copy a site from one pool to another.
 *
 * @param string $from_url
 *   The URL of the site to copy from.
 * @param string $to
 *   The pool or URL to copy to.
 * @param bool $create_skeleton
 *   When TRUE, run create_skeleton() before copying.
 */
function site_copy_full($from_url, $to, $create_skeleton = TRUE) {
  $from = parse_site_url($from_url);
  $from_pool = $from['pool'];
  $from_url_path = $from['url_path'];

  // Copy without rename.
  if (valid_pool($to)) {
    $to_pool = $to;
    $to_url_path = $from_url_path;
  }
  // Copy to URL which may contain rename.
  else {
    try {
      $to = parse_site_url($to);
    }
    catch (Exception $e) {
      throw new Exception('Invalid "to"; must be pool or URL.');
    }
    $to_pool = $to['pool'];
    $to_url_path = $to['url_path'];
  }

  if ($from_pool === $to_pool && $from_url_path === $to_url_path) {
    throw new Exception('"from" and "to" are the same place.');
  }

  if ($from_url_path !== $to_url_path && site_makefile_exists($from_url_path) && !site_makefile_exists($to_url_path)) {
    throw new Exception('Site has a makefile. You must rename it before copying so that the skeleton will have the makefile built in it.');
  }

  $to_url = build_site_url($to_pool, $to_url_path);
  $profile_version_name = get_profile_version($from_pool, $from_url_path);

  echo 'Starting copying ';
  // Copy site between pools.
  if ($from_url_path === $to_url_path) {
    echo "$from_url_path from $from_pool to $to_pool";
  }
  // Copy to new page on same pool.
  elseif ($from_pool === $to_pool) {
    echo "$from_url_path to $to_url_path on $from_pool";
  }
  // Pool and path both different.
  else {
    echo "$from_pool/$from_url_path to $to_pool/$to_url_path";
  }
  echo "...\n";

  drush_site_remove_missing_modules($from_pool, $from_url_path);

  if ($create_skeleton) {
    $settings = parse_settings_file($from_pool, $from_url_path);
    create_skeleton($to_url, $profile_version_name, [], (bool) $settings['utf8mb4']);
  }
  else {
    // Test for site existance.
    get_drupal_root($to_pool, $to_url_path);
  }

  // Copy alias to new skeleton.
  $config_apache = get_site_config_apache($from_pool, $from_url_path);
  if ($config_apache['aliases']) {
    echo "Creating alias \"" . $config_apache['aliases'][0] . "\" ...\n";
    create_canonical_alias($to_url, $config_apache['aliases'][0]);
  }

  copy_site_files($from_pool, $to_pool, $from_url_path, $to_url_path);

  if ($from_url_path !== $to_url_path) {
    echo "Skeleton created and files copied. Database must be copied with rename manually using db-dump.php, db-site-rename.php, and db-import.php. Then run site-adjust.php.\n";
    return;
  }

  db_copy($from_url, $to_pool);

  adjust_site($to_pool, $to_url_path);
  drush_site_cache_clear($to_pool, $to_url_path);

  site_is_up($to_pool, $to_url_path);
}

/**
 * Delete the database and all files for a site.
 *
 * @param string $site_url
 *   The URL of the site to delete.
 * @param bool $make_backup
 *   When TRUE, backup the database and settings file.
 */
function delete_site($site_url, $make_backup = TRUE) {
  echo "Starting deleting site...\n";

  $site_url = parse_site_url($site_url);
  $pool = $site_url['pool'];
  $url_path = $site_url['url_path'];
  exclude_root_site($url_path);
  // Set $drupal_root even if get_drupal_root() fails.
  try {
    $drupal_root = get_drupal_root($pool, $url_path);
  }
  catch (Exception $e) {
    $drupal_root = NULL;
  }

  $pool_info = pool_info();
  $engine = $pool_info[$pool]['engine'];
  $dbmach = $pool_info[$pool]['dbmach'];
  $file_path = url_path_to_file_path($url_path);

  // Clear cache so that it will not continue to show up after deletion.
  echo "Clearing varnish cache...\n";
  try {
    drush_command('cache-clear varnish', $pool, $url_path);
  }
  catch (Exception $e) {
    echo 'Cache not cleared (site probably doesn\'t exist): ' . $e->getMessage() . "\n";
  }

  // Drop the user and database on the database hosts.
  // Parse settings file before it is deleted.
  echo "Deleting database...\n";
  try {
    $settings = parse_settings_file($pool, $url_path);
  }
  catch (Exception $e) {
    echo 'WARNING: ' . $e->getMessage() . " Nothing done to database.\n";
    $settings = NULL;
  }
  $backup_dir = '/wmsfiles/backup/';
  if ($settings) {
    if ($make_backup) {
      // Backup database, if it exists.
      $backup_filename = $backup_dir . $settings['UWdb'] . '.sql';
      echo 'Saving database dump as: ' . $backup_filename . "\n";
      $dump_filename = NULL;
      try {
        $dump_filename = db_dump($pool, $url_path, $pool, get_tempdir())['filtered'];
      }
      catch (Exception $e) {
      }
      if ($dump_filename) {
        rename($dump_filename, $backup_filename);
      }
      remove_tempdir();
    }

    // Delete database and user.
    $sql = "DROP DATABASE IF EXISTS " . $settings['UWdb'] . ";
      DROP USER IF EXISTS '" . $settings['UWuser'] . "'@'" . $dbmach . ".uwaterloo.ca';
      FLUSH PRIVILEGES;";
    system_command('mysql --host=' . escapeshellarg($settings['dbhost']) . ' --execute=' . escapeshellarg($sql));

  }

  // Backup settings.php.
  if ($drupal_root && $make_backup) {
    $source_filename = $drupal_root . '/sites/' . $file_path . '/settings.php';
    $backup_filename = $backup_dir . $pool . '.' . $file_path . '-' . date('Y-m-d') . '-settings.php';
    echo 'Backing-up settings.php in: ' . $backup_filename . "\n";
    exec('rsync ' . escapeshellarg(WORKER . '@' . $engine[0] . ':' . $source_filename) . ' ' . escapeshellarg($backup_filename), $output);
    echo implode("\n", $output) . "\n";
  }

  // Delete site files.
  foreach ($engine as $eng) {
    echo "Deleting $url_path on $eng...\n";
    // 'shredsite' must delete everything created in create_skeleton().
    ssh_exec($eng, 'sudo shredsite ' . escapeshellarg($file_path));
  }
}

/**
 * Return an array of the pools that this site exists on.
 *
 * @param string $url_path
 *   The URL path of the site to rebuild.
 *
 * @return array
 *   An array of the names of the pools that this site exists on.
 */
function find_site_pools($url_path) {
  if (!valid_url_path($url_path)) {
    throw new Exception('Invalid URL path.');
  }
  $pools_with_site = array();
  foreach (pool_info() as $pool => $pool_info) {
    if (empty($pool_info['secure']) && get_profile_version($pool, $url_path)) {
      $pools_with_site[] = $pool;
    }
  }
  return $pools_with_site;
}

/**
 * Get the URL path for an alias.
 *
 * @param string $alias
 *   A URL path which may be an alias.
 *
 * @return string|null
 *   If $alias is an alias on live, the URL path it refers to, NULL otherwise.
 */
function get_alias_url_path($alias) {
  $pool_info = pool_info();

  $command = 'grep --no-filename --recursive ' . escapeshellarg('RedirectPermanent /' . $alias . ' ') . ' ' . APACHE_DIR . '/builds/uw_base_profile-*/aliases*';
  $alias = ssh_exec($pool_info['live']['engine'][0], $command);
  if ($alias) {
    $alias = explode(' ', $alias, 3);
    return substr($alias[2], 1);
  }
}

/**
 * Output information about a site.
 *
 * Output a list of all pools on which a site has a site directory and the
 * status of its associated database.
 *
 * @param string $url_path
 *   The URL path of the site.
 */
function site_info($url_path) {
  if (!valid_url_path($url_path)) {
    throw new Exception('Invalid url_path.');
  }

  echo 'Site makefile: ';
  $makefile = site_makefile_exists($url_path);
  if ($makefile) {
    echo 'Yes';
  }
  elseif ($makefile === FALSE) {
    echo 'No';
  }
  else {
    echo 'Error: Unable to contact Gitlab.';
  }
  echo "\n";

  // Output if the path is an alias on live.
  $alias = get_alias_url_path($url_path);
  if ($alias) {
    echo "\n";
    echo 'Path is an alias for: ' . $alias . "\n";
  }

  $pool_info = pool_info();
  $pools = find_site_pools($url_path);
  foreach ($pools as $pool) {
    echo "\n";
    echo build_site_url($pool, $url_path) . "\n";
    $profile_version = get_profile_version($pool, $url_path);
    echo "\tProfile: " . $profile_version . "\n";

    $profile = parse_profile_version($profile_version);

    try {
      $settings = parse_settings_file($pool, $url_path);
    }
    catch (Exception $e) {
      echo 'Error: ' . $e->getMessage() . "\n";
      $settings = NULL;
    }

    if ($settings) {
      echo "\tDatabase host: " . $settings['dbhost'] . "\n";
      echo "\tDatabase name: " . $settings['UWdb'];
      if (database_exists($settings['dbhost'], $settings['UWdb'])) {
        if ($settings['UWdb'] !== url_path_to_db_name($pool, $url_path, $profile['drupal_major_version_number'])) {
          echo ' WARNING: Non-standard name';
        }
      }
      else {
        echo ' WARNING: Database missing';
      }
      echo "\n";
      echo "\tDatabase 4 byte UTF-8: " . ($settings['utf8mb4'] ? 'Yes' : 'No') . "\n";
    }

    if (drush_command('sql-query "SHOW TABLES"', $pool, $url_path)) {
      echo "\tTheme: " . (get_site_theme($pool, $url_path) ?: 'Unable to detect.') . "\n";
    }
    else {
      echo "\tDrupal not installed.\n";
    }

    foreach ($pool_info[$pool]['engine'] as $engine) {
      if ($makefile && !remote_file_exists($engine, get_drupal_root($pool, $url_path) . '/sites/' . url_path_to_file_path($url_path) . '/makefile/site.make')) {
        echo "\tERROR: Site makefile missing";
        if (count($pool_info[$pool]['engine']) > 1) {
          echo ' from ' . $engine;
        }
        echo ".\n";
      }
    }

    foreach (get_site_config_apache($pool, $url_path) as $type => $paths) {
      if ($type === 'decoupled') {
        echo "\t" . ucfirst($type) . ': ';
        if ($paths) {
          echo 'Yes';
        }
        elseif ($paths === FALSE) {
          echo 'No';
        }
        else {
          echo 'Unable to detect';
        }
        echo "\n";
      }
      else {
        if ($paths) {
          echo "\t" . ucfirst($type) . ': ' . implode(', ', $paths) . "\n";
        }
      }
    }
  }
}

/**
 * Move a site to a different profile.
 *
 * @param string $site
 *   The URL of the site.
 * @param string $new_profile
 *   The name of the profile to move the site to.
 */
function site_change_profile($site, $new_profile) {
  echo "Starting changing site profile.\n";

  $url_path = NULL;
  try {
    $url_path = file_path_to_url_path($site);
  }
  catch (Exception $e) {
  }
  if (isset($url_path)) {
    // Cannot use $site directly because it might end in a slash.
    $file_path = url_path_to_file_path($url_path);
    $pool = get_pool();
    if (!valid_pool($pool)) {
      throw new Exception('Current host is not in a pool.');
    }
  }
  else {
    $site = parse_site_url($site);
    $file_path = $site['file_path'];
    $url_path = $site['url_path'];
    $pool = $site['pool'];
  }
  exclude_root_site($url_path);

  $old_profile = get_profile_version($pool, $url_path);
  if (!$old_profile) {
    throw new Exception('Site does not exist.');
  }

  if (!($profile = parse_profile_version($new_profile))) {
    throw new Exception('Invalid profile name. Must be like "<profile_name>-<drupal_version>-<profile_version>".');
  }

  $new_profile = $profile['profile_version_name'];

  echo "Changing $url_path on $pool from $old_profile to $new_profile...\n";

  if ($old_profile === $new_profile) {
    echo "Success: Site is already on that profile.\n";
    return;
  }

  $pool_info = pool_info();
  if (!valid_drupal_root($pool_info[$pool]['engine'][0], WEB_ROOT . '/' . $new_profile)) {
    throw new Exception('Profile does not exist on host.');
  }

  // Ensure permissions are correct before proceeding.
  tweaksite($pool, $url_path);

  // On each host, move the site's config to the new profile.
  $sites_path_old = WEB_ROOT . '/' . $old_profile . '/sites/';
  $sites_path_new = WEB_ROOT . '/' . $new_profile . '/sites/';
  foreach ($pool_info[$pool]['engine'] as $engine) {
    echo "Moving $url_path from $old_profile to $new_profile on $engine...\n";
    ssh_exec($engine, 'mv `find -L ' . $sites_path_old . ' -maxdepth 1 -samefile ' . $sites_path_old . $file_path . '` ' . $sites_path_new, $output, $return_var);
    if (!empty($return_var)) {
      throw new Exception('Unable to move site directory.');
    }
  }

  echo "Updating Apache configuration...\n";
  $commands = [];
  $path = APACHE_DIR . '/builds/';
  $file = site_level($url_path) . '/' . $file_path . '.Pconf';
  // Move the site's Apache config from the old profile directory to the new.
  foreach (['aliases', 'rewrites'] as $type) {
    $commands[] = 'mv ' . escapeshellarg($path . $old_profile . '/' . $type . $file) . ' ' . escapeshellarg($path . $new_profile . '/' . $type . $file);
  }
  // Replace the old profile version with the new in the new.
  $commands[] = 'sed --in-place=.old --expression="s/' . $old_profile . '/' . $new_profile . '/" ' . escapeshellarg($path . $new_profile . '/aliases' . $file);
  $commands = implode(' && ', $commands);
  ssh_exec($pool_info[$pool]['engine'][0], $commands, $output, $return_var);
  if (!empty($return_var)) {
    throw new Exception('Unable to update Apache configuration.');
  }
  apache_restart($pool);

  // Clear profile version cache.
  get_profile_version($pool, $url_path, TRUE);

  drush_site_update_clear_cache($pool, $url_path);

  // Extra cache clear for decoupled sites.
  if (get_site_is_decoupled($pool, $url_path)) {
    drush_site_cache_clear($pool, $url_path);
  }

  site_is_up($pool, $url_path);
}

/**
 * Use Drush to run site updates, cache clears, and feature reverts.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 */
function drush_site_update_clear_cache($pool, $url_path) {
  // Run upgrades and clear caches.
  echo "Flushing registry...\n";
  drush_command('cache-clear registry', $pool, $url_path);
  echo "Running update hooks...\n";
  drush_command('updatedb --yes', $pool, $url_path);
  drush_site_cache_clear($pool, $url_path);
  drush_site_features_revert_all($pool, $url_path);
  drush_site_remove_missing_modules($pool, $url_path);
}

/**
 * Run Drush "cache-clear all" on a site.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 */
function drush_site_cache_clear($pool, $url_path) {
  echo "Clearing cache...\n";
  drush_command('cache-clear all', $pool, $url_path);
}

/**
 * Run Drush features-revert-all on a site.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 */
function drush_site_features_revert_all($pool, $url_path) {
  echo "Reverting features...\n";
  drush_command('features-revert-all --yes', $pool, $url_path);
}

/**
 * Run Drush remove-missing-modules on a site.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 */
function drush_site_remove_missing_modules($pool, $url_path) {
  echo "Running remove-missing-modules...\n";
  drush_command('remove-missing-modules --yes', $pool, $url_path);
}

/**
 * Check if a site is working.
 *
 * Output a message and return the HTTP status.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 *
 * @return bool
 *   Whether the site is up.
 */
function site_is_up($pool, $url_path) {
  // Make request to site homepage.
  $context = stream_context_create([
    'http' => [
      'ignore_errors' => TRUE,
    ],
  ]);
  $page = file_get_contents(
    build_site_url($pool, $url_path),
    FALSE,
    $context
  );

  // Report site down if it does not return status 200 or 403. Sites that
  // require authentication will return 403.
  if (isset($http_response_header)) {
    $status = parse_http_response_header_status($http_response_header);
  }
  else {
    $status = error_get_last()['message'];
  }
  if (!in_array($status, [200, 403])) {
    echo "DevOps Error: Site appears to be down (HTTP status " . $status . ").\n";
    return FALSE;
  }

  // Most sites will pass both test. Site with non-Latin URLs will only pass
  // the first test. Decoupled sites will only pass the second.
  $test = strpos($page, '/sites/' . url_path_to_file_path($url_path) . '/files/') !== FALSE || strpos($page, '/' . $url_path . '/cas?') !== FALSE;

  if ($test) {
    echo 'Success: Site appears to be up.';
  }
  else {
    echo 'DevOps Error: Site appears to be down (no WCMS content).';
  }
  echo "\n";
  return $test;
}

/**
 * Make adjustments to a site needed after copying or creation.
 *
 * Enable or disable modules to match the desired configuration for the pool.
 * Rebuild the sitemap.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 */
function adjust_site($pool, $url_path) {
  echo "Adjusting site modules...\n";

  $pool_info = pool_info();
  $action = $pool_info[$pool]['varnish'] ? 'enable' : 'disable';
  $command = 'pm-' . $action . ' --yes varnish expire uw_cfg_expire';

  drush_command($command, $pool, $url_path);
  drush_site_remove_missing_modules($pool, $url_path);

  drush_command('xmlsitemap-rebuild', $pool, $url_path);
}
