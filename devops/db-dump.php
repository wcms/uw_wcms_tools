#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'db-dump.php FROM-SITE-URL TO-POOL DUMP-DIR';
min_args($argv, 3);

try {
  $site = parse_site_url($argv[1]);
  db_dump($site['pool'], $site['url_path'], $argv[2], $argv[3]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
