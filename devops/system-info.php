#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'system-info.php [POOL]';
min_args($argv, 0);

try {
  system_info(isset($argv[1]) ? $argv[1] : NULL);
}
catch (Exception $e) {
  msg($e->getMessage());
}
