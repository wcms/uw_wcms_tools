#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'db-import.php SITE-URL DUMP-FILENAME';
min_args($argv, 2);

try {
  $site = parse_site_url($argv[1]);
  db_import($site['pool'], $site['url_path'], $argv[2]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
