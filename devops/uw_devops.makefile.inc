<?php

/**
 * @file
 * Functions relating to makefiles.
 */

/**
 * Verify the makefiles of a site or sites.
 *
 * @param string $to_check
 *   What to check, either the name of a pool or a site URL.
 *
 * @return null
 *   The output of makefile_verify_pool|site().
 */
function makefile_verify($to_check) {
  if (valid_pool($to_check)) {
    return makefile_verify_pool($to_check);
  }
  else {
    $site_url = parse_site_url($to_check);
    $pool = $site_url['pool'];
    $url_path = $site_url['url_path'];
    return makefile_verify_site($pool, $url_path);
  }
}

/**
 * Verify the makefiles of a single site.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 */
function makefile_verify_site($pool, $url_path) {
  echo "Checking makefile for $url_path on $pool...\n";
  $not_empty = makefile_invalid($pool, $url_path);

  echo "\n";
  if ($not_empty === TRUE) {
    echo 'No makefile, but directories are not empty.';
  }
  elseif ($not_empty === FALSE) {
    echo 'No makefile, no problems found.';
  }
  elseif ($not_empty) {
    echo 'Site directory does not match makefile: ' . implode(', ', $not_empty);
  }
  else {
    echo 'Has makefile, no problems found.';
  }
  echo "\n";
}

/**
 * Verify the makefiles of all the sites on a pool.
 *
 * @param string $pool
 *   The pool to check.
 */
function makefile_verify_pool($pool) {
  $sites = list_sites_by_pool($pool);
  $errors = [];
  $no_makefile_not_empty = [];
  foreach ($sites as $url_path => $profile) {
    echo "Checking makefile for $url_path...\n";
    $not_empty = makefile_invalid($pool, $url_path, $profile);
    if ($not_empty === TRUE) {
      $no_makefile_not_empty[] = $profile . '/' . $url_path;
    }
    elseif ($not_empty) {
      $errors[] = $profile . '/' . $url_path . ': ' . implode(', ', $not_empty);
    }
  }

  echo "\n";
  echo 'Site directory does not match makefile:';
  if ($errors) {
    echo "\n" . implode("\n", $errors);
  }
  else {
    echo ' None.';
  }
  echo "\n";

  echo "\n";
  echo 'No makefile, but directories are not empty:';
  if ($no_makefile_not_empty) {
    echo "\n" . implode("\n", $no_makefile_not_empty);
  }
  else {
    echo ' None.';
  }
  echo "\n";
}

/**
 * Verify the a site's makefile is properly built.
 *
 * @param string $pool
 *   The pool of the site.
 * @param string $url_path
 *   The URL path of the site.
 * @param string $profile_version_name
 *   If provided, this will be used to determine the drupal_root instead of
 *   looking it up, making it run faster.
 *
 * @return array|bool
 *   If there is no makefile, return FALSE if there is no problem or TRUE if the
 *   directories are not empty. Otherwise, return an array of error messages,
 *   which is empty if there is no problem.
 */
function makefile_invalid($pool, $url_path, $profile_version_name = NULL) {
  $site_makefile_exists = site_makefile_exists($url_path);

  // Lookup the drupal_root root if the profile version is not provided.
  if ($profile_version_name) {
    $drupal_root = WEB_ROOT . '/' . $profile_version_name;
  }
  else {
    $drupal_root = get_drupal_root($pool, $url_path);
  }

  $pool_info = pool_info();
  $engine = $pool_info[$pool]['engine'][0];
  $file_path = url_path_to_file_path($url_path);
  $site_dir = $drupal_root . '/sites/' . $file_path;

  $directories_to_test = ['modules', 'themes', 'libraries'];

  // No makefile, check that all directories are empty.
  if ($site_makefile_exists === FALSE) {
    ssh_exec($engine, 'find -- ' . escapeshellarg($site_dir) . '/{' . implode(',', $directories_to_test) . '} -prune -type d -empty 2>&1', $output);
    return count($output) < count($directories_to_test);
  }
  // Makefile exists, check that it matches what is on the site.
  elseif ($site_makefile_exists === TRUE) {
    $invalid = [];
    $tempdir = ssh_exec($engine, 'mktemp --directory --tmpdir uw_devops.XXXXXXXXXX');
    try {
      makefile_build($url_path, $engine, $tempdir);
    }
    catch (Exception $e) {
      $invalid[] = 'Makefile does not build.';
      return $invalid;
    }
    foreach ($directories_to_test as $dir) {
      $site_subdir = $site_dir . '/' . $dir;
      $temp_subdir = $tempdir . '/' . $dir;
      // makefile_build() always builds all the sub-directories. Only check
      // those that also exist in the site directory.
      if (remote_dir_exists($engine, $site_subdir)) {
        $has_differences = ssh_exec($engine, 'diff --brief --exclude=".git" --exclude="*.info" --recursive ' . $temp_subdir . ' ' . $site_subdir);
        if ($has_differences) {
          $invalid[] = $dir . ' differs';
        }
      }
      // If a sub-directory doesn't exist, only complain if the makefile version
      // is not empty.
      elseif (remote_dir_not_empty($engine, $temp_subdir)) {
        $invalid[] = $dir . ' empty';
      }
    }
    if ($invalid) {
      array_unshift($invalid, 'Has makefile');
    }
    ssh_exec($engine, 'rm -rf ' . $tempdir);
  }

  return $invalid;
}

/**
 * Return the URL of a site's makefile on Gitlab.
 *
 * @param string $url_path
 *   The URL path of the site.
 *
 * @return string|null
 *   The URL of the site's makefile or NULL if it does not exist.
 */
function site_makefile_url($url_path) {
  $makefile = site_makefile_get($url_path);

  if (!$makefile) {
    return;
  }

  $project = uw_wcms_tools_get_project('wcms-sites', url_path_to_repository_path($url_path));

  return $project->web_url . '/raw/' . $makefile->ref . '/site.make';
}

/**
 * Get a site makefile from Gitlab.
 *
 * It will check the greatest tag using version_compare() or, if there are no
 * tags, the default branch.
 *
 * @param string $url_path
 *   The URL path of the site.
 *
 * @return object|null
 *   The makefile object if the site has one. Otherwise, NULL.
 */
function site_makefile_get($url_path) {
  static $cache = [];
  if (array_key_exists($url_path, $cache)) {
    return $cache[$url_path];
  }

  if (!valid_url_path($url_path)) {
    throw new Exception('Invalid site URL path.');
  }

  require_once __DIR__ . '/../uw_wcms_tools.lib.inc';

  // Get information about the project.
  $project = uw_wcms_tools_get_project('wcms-sites', url_path_to_repository_path($url_path));
  if (!$project) {
    $cache[$url_path] = NULL;
    return;
  }

  // Get the ref, either the latest tag or the default branch.
  $ref = uw_wcms_tools_get_tag_latest($project->id, NULL, TRUE);
  $ref = $ref ? $ref->name : $project->default_branch;

  // Check if the makefile exists at the ref.
  $query = [
    'ref' => $ref,
  ];
  $file = uw_wcms_tools_query_gitlab_api('projects/' . $project->id . '/repository/files/site.make', $query, 'HEAD');

  if (isset($file['body'])) {
    $cache[$url_path] = $file['body'];
  }
  else {
    $cache[$url_path] = NULL;
  }
  return $cache[$url_path];
}

/**
 * Test whether or not a site has a site makefile on Gitlab.
 *
 * @param string $url_path
 *   The URL path of the site.
 *
 * @return bool
 *   TRUE of the site has a makefile, FALSE if it does not.
 */
function site_makefile_exists($url_path) {
  return (bool) site_makefile_get($url_path);
}

/**
 * Rebuild the makefile for a site using drush-make.
 *
 * If there are any tags, it will rebuild using the latest tag, otherwise, HEAD
 * of the default branch.
 *
 * @param string $site_url
 *   The URL of the site.
 *
 * @see makefile_build()
 */
function site_makefile_rebuild($site_url) {
  echo "Starting site makefile rebuild...\n";

  $site_url = parse_site_url($site_url);
  $url_path = $site_url['url_path'];
  $pool = $site_url['pool'];

  // Ensure permissions are correct before attempting to delete anything.
  tweaksite($pool, $url_path);

  $site_dir = get_drupal_root($pool, $url_path, TRUE) . '/sites/' . url_path_to_file_path($url_path);

  $pool_info = pool_info();
  foreach ($pool_info[$pool]['engine'] as $host) {
    makefile_build($url_path, $host, $site_dir);
  }
}

/**
 * Build a site makefile in a specified location using drush-make.
 *
 * If there are any tags, it will rebuild using the latest tag, otherwise, HEAD
 * of the default branch.
 *
 * @param string $url_path
 *   The URL path of the site makefile to build.
 * @param string $host
 *   The host to build on.
 * @param string $destination_dir
 *   The directory to build in.
 *
 * @see site_makefile_rebuild()
 */
function makefile_build($url_path, $host, $destination_dir) {
  echo 'Starting building makefile for ' . $url_path . ' on ' . $host . "...\n";

  if (!remote_dir_exists($host, $destination_dir)) {
    throw new Exception("Unable to build makefile: Directory $destination_dir does not exist on $host.");
  }

  $site_sub_dirs = ['modules', 'themes', 'libraries'];

  $command = array();
  $command[] = 'cd ' . $destination_dir;

  $makefile = site_makefile_get($url_path);

  // If there is no makefile, remove everyting.
  if (!$makefile) {
    echo "No site makefile. Removing all local modules, etc...\n";
    $command[] = 'rm -rf makefile ' . implode('/* ', $site_sub_dirs) . '/*';
    $command[] = 'mkdir --parents ' . implode(' ', $site_sub_dirs);
    $command = implode(' && ', $command);
    ssh_exec($host, $command, $output, $return_var);
    echo "Complete.\n";
    return;
  }

  $command[] = 'rm -rf makefile';
  // Clone branch and checkout the latest tag is there is one.
  $command[] = 'git clone --branch ' . escapeshellarg($makefile->ref) . ' https://git.uwaterloo.ca/wcms-sites/' . url_path_to_repository_path($url_path) . '.git makefile';

  // Build the makefile off to the side.
  $command[] = 'drush make --working-copy --force-gitinfofile --no-recursion --no-core --yes --contrib-destination=rebuild makefile/*.make';
  $command = implode(' && ', $command);

  ssh_exec($host, $command, $output, $return_var);
  if ($return_var !== 0) {
    throw new Exception("Unable to build makefile: 'drush make' returned non-zero exit status.");
  }

  // Move the newly-built files into position.
  $command = [];
  $command[] = 'cd ' . $destination_dir;
  $command[] = 'rm -rf ' . implode('/* ', $site_sub_dirs) . '/*';
  $command[] = 'mv rebuild/* .';
  $command[] = 'rm -rf rebuild';
  ssh_exec($host, implode(' && ', $command), $output, $return_var);

  echo "Copying forms configuration...\n";
  $forms_dir = $destination_dir . '/makefile/forms';
  $command = array();
  // Remove existing forms config.
  $command[] = 'rm --recursive --force ' . $destination_dir . '/forms';
  // Copy new forms config if it exists.
  if (remote_dir_exists($host, $forms_dir)) {
    $command[] = 'cp --recursive ' . $forms_dir . ' ' . $destination_dir;
  }
  $command = implode(' && ', $command);
  ssh_exec($host, $command);

  echo "Building makefile complete.\n";
}
