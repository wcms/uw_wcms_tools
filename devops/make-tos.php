#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'make-tos.php';
min_args($argv, 0);

try {
  make_tos();
}
catch (Exception $e) {
  msg($e->getMessage());
}
