#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'create-site.php SITE-URL PROFILE-VERSION [SITE-NAME]
PROFILE-VERSION must be like "<profile_name>-<drupal_version>-<profile_version>".
Deprecated in favour of site-create.php.';
min_args($argv, 2);

try {
  create_site($argv[1], $argv[2], isset($argv[3]) ? ['--site-name=' . $argv[3]] : NULL);
}
catch (Exception $e) {
  msg($e->getMessage());
}
