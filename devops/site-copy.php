#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'site-copy.php FROM-SITE-URL TO-POOL|TO-URL
If TO-URL is used and the site-path changes, the database copy must be done manually.';
min_args($argv, 2);

try {
  site_copy_full($argv[1], $argv[2]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
