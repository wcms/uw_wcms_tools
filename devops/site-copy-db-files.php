#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'site-copy-db-files.php FROM-SITE-URL TO-POOL

Copies the database and files to an existing skeleton or site.';
min_args($argv, 2);

try {
  site_copy_full($argv[1], $argv[2], FALSE);
}
catch (Exception $e) {
  msg($e->getMessage());
}
