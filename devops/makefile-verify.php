#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'makefile-verify.php POOL|SITE-URL
Checks that the makefile matches what is in the site directory for either an
entire pool or a single site';
min_args($argv, 1);

try {
  makefile_verify($argv[1]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
