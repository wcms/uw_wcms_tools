#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'host-to-pool.php [HOST]
Display the pool of a given host or the current host if blank.';
min_args($argv, 0);

try {
  if (isset($argv[1])) {
    $host = $argv[1];
  }
  else {
    $host = NULL;
  }
  echo get_pool($host) . "\n";
}
catch (Exception $e) {
  msg($e->getMessage());
}
