#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'change-site-profile.php NEW-PROFILE SITE...
Moves one or more sites to NEW-PROFILE.

SITE can be either a URL or the site directory, such as "ca.site-path". In the
latter case, this script must be run on a web server in the site\'s pool.';
min_args($argv, 2);

// Remove name of script.
array_shift($argv);
// Get profile version.
$profile = array_shift($argv);

// Rest of arguments are sites.
foreach ($argv as $site) {
  try {
    site_change_profile($site, $profile);
  }
  catch (Exception $e) {
    msg($e->getMessage());
  }
}
