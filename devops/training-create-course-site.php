#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'training-create-course-site.php COURSE USERNAME...';
min_args($argv, 2);

// Remove name of script.
array_shift($argv);
// Get course identifyer.
$course = array_shift($argv);

try {
  create_training_course_sites($course, $argv);
}
catch (Exception $e) {
  msg($e->getMessage());
}
