#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'site-copy-files.php FROM-SITE-URL TO-POOL
site-copy-files.php FROM-SITE-URL TO-SITE-URL';
min_args($argv, 2);

try {
  copy_site_files_url($argv[1], $argv[2]);
}
catch (Exception $e) {
  msg($e->getMessage());
}
