#!/usr/bin/env php
<?php

/**
 * @file
 * Command-line script.
 */

require_once 'uw_devops.inc';
global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'create-redirect.php SITE-URL REDIRECT...';
min_args($argv, 2);

try {
  // Remove name of command.
  array_shift($argv);
  $site_url = array_shift($argv);
  // Rest are the aliases.
  $redirects = $argv;
  create_redirects($site_url, $redirects);
}
catch (Exception $e) {
  msg($e->getMessage());
}
