#!/usr/bin/env php
<?php

/**
 * @file
 * Create semver branches for any 8.x branches in GitLab projects.
 */

require_once 'devops/uw_devops.inc';
require_once 'uw_wcms_tools.gitlab.inc';

global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'gitlab-make-semver.php GITLAB-PATH
Create semver branches for any 8.x branches. Move the default to semver. Provide
either a GITLAB-PATH, such as "wcms/uw_base_profile" or "wcms" to do all in that
group.';
min_args($argv, 1);

// Separate GITLAB-PATH.
$arg = explode('/', $argv[1], 2);

// Single project.
if (count($arg) === 2) {
  $project = uw_wcms_tools_get_project($arg[0], $arg[1]);
  if (!$project) {
    throw new Exception('Invalid project.');
  }
  $projects = [$project];
}
// All projects in a group.
else {
  $group = $arg[0];
  $projects = uw_wcms_tools_get_projects($group)[$group];
}

foreach ($projects as $project) {
  echo "\n";
  echo $project->path_with_namespace . "\n";
  uw_wcms_tools_create_semver($project);
}
