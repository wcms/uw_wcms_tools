#!/usr/bin/env php
<?php

/**
 * @file
 * Tag releases for GitLab projects.
 */

require_once 'devops/uw_devops.inc';
require_once 'uw_wcms_tools.gitlab.inc';

global $_uw_wcms_tools_usage;
$_uw_wcms_tools_usage = 'gitlab-tag-release.php GITLAB-PATH|all
Tag a release of a GitLab project. Provide a GITLAB-PATH, such as
"wcms/uw_base_profile" or "all" to tag all WCMS projects in the profile.';
min_args($argv, 1);

if ($argv[1] === 'all') {
  $projects = uw_wcms_tools_gitlab_get_profile_projects();
  echo 'Tagging ' . count($projects) . " projects...\n";
}
else {
  // Separate GITLAB-PATH.
  $arg = explode('/', $argv[1], 2);

  $projects = [
    ['namespace' => $arg[0], 'path' => $arg[1]],
  ];
}

foreach ($projects as $project) {
  uw_wcms_tools_gitlab_tag_release($project['namespace'], $project['path']);
}
